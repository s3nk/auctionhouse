package test.technique.AuctionHouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The object was not found (note : can only be applied if the missing object identifier is specified in the URL)
 * HttpCode 404 with className and identifier
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {
    private Object identifier;
    private Class className;

    public NotFoundException(Object identifier, Class className, String message) {
        super(message);
        this.identifier = identifier;
        this.className = className;
    }

    public Object getIdentifier() {
        return identifier;
    }
    public Class getClassName() {
        return className;
    }
}
