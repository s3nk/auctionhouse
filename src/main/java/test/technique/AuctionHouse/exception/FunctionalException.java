package test.technique.AuctionHouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Something went wrong, but it was a known error case
 * HttpCode 400 with code and message
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FunctionalException extends Exception {
    private String code;

    public FunctionalException(String message, String code) {
        super(message);
        this.code = code;
    }

    public FunctionalException() {
    }

    public String getCode() {
        return code;
    }
}
