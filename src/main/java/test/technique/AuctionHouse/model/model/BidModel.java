package test.technique.AuctionHouse.model.model;

import java.math.BigDecimal;
import java.util.Date;

public class BidModel {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Auction id
     */
    private Long auctionId;

    /**
     * User id
     */
    private Long userId;

    /**
     * Amount
     */
    private BigDecimal amount;

    /**
     * Deletion date
     */
    private Date deletionDate;

    /**
     * Creation date
     */
    private Date creationDate;

    public BidModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
