package test.technique.AuctionHouse.model.model;

import test.technique.AuctionHouse.model.enumeration.AuctionStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public class AuctionModel {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Auction house id
     */
    private Long auctionHouseId;

    /**
     * Display name
     */
    private String name;

    /**
     * Description
     */
    private String description;

    /**
     * Start date
     */
    private LocalDateTime startDate;

    /**
     * End date
     */
    private LocalDateTime endDate;

    /**
     * Start price
     */
    private BigDecimal startPrice;

    /**
     * Creation date
     */
    private Date creationDate;

    /**
     * Deletion date
     */
    private Date deletionDate;

    public AuctionModel() {
    }

    /**
     * Return the current auction status according the the dates
     * @return The current auction status
     */
    public AuctionStatus getStatus() {
        LocalDateTime now = LocalDateTime.now();
        if (deletionDate != null) {
            return AuctionStatus.DELETED;
        }
        if (startDate.isAfter(now)) {
            return AuctionStatus.NOT_STARTED;
        } else {
            if (endDate.isAfter(now)) {
                return AuctionStatus.RUNNING;
            } else {
                return AuctionStatus.TERMINATED;
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuctionHouseId() {
        return auctionHouseId;
    }

    public void setAuctionHouseId(Long auctionHouseId) {
        this.auctionHouseId = auctionHouseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }
}
