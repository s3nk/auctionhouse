package test.technique.AuctionHouse.model.model;

import java.util.Date;

public class AuctionHouseModel {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Display name
     */
    private String name;

    /**
     * Creation date
     */
    private Date creationDate;

    /**
     * Deletion date, obv. nullable
     */
    private Date deletionDate;

    public AuctionHouseModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }
}
