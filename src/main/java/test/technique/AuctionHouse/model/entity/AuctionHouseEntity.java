package test.technique.AuctionHouse.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "auction_house")
public class AuctionHouseEntity {
    /**
     * Unique ID, generated with sequence
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auction_house_generator")
    @SequenceGenerator(name="auction_house_generator", sequenceName = "auction_house_seq")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    /**
     * Display name
     */
    private String name;

    /**
     * Creation date (Hibernate)
     */
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * Deletion date, obv. nullable
     */
    @Column(name = "deletion_date")
    private Date deletionDate;

    public AuctionHouseEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }
}
