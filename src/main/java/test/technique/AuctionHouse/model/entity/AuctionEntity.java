package test.technique.AuctionHouse.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "auction")
public class AuctionEntity {
    /**
     * Unique ID, generated with sequence
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auction_generator")
    @SequenceGenerator(name="auction_generator", sequenceName = "auction_seq")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    /**
     * Auction house id
     */
    @Column(name = "auction_house_id")
    private Long auctionHouseId;

    /**
     * Display name
     */
    private String name;

    /**
     * Description
     */
    private String description;

    /**
     * Start date
     */
    @Column(name = "start_date")
    private LocalDateTime startDate;

    /**
     * End date
     */
    @Column(name = "end_date")
    private LocalDateTime endDate;

    /**
     * Start price
     */
    @Column(name = "start_price")
    private BigDecimal startPrice;

    /**
     * Creation date (Hibernate)
     */
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * Deletion date, obv. nullable
     */
    @Column(name = "deletion_date")
    private Date deletionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuctionHouseId() {
        return auctionHouseId;
    }

    public void setAuctionHouseId(Long auctionHouseId) {
        this.auctionHouseId = auctionHouseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }
}
