package test.technique.AuctionHouse.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "bid")
public class BidEntity {
    /**
     * Unique ID, generated with sequence
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bid_generator")
    @SequenceGenerator(name="bid_generator", sequenceName = "bid_seq")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    /**
     * Auction id
     */
    @Column(name = "auction_id")
    private Long auctionId;

    /**
     * User id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * Amount
     */
    private BigDecimal amount;

    /**
     * Deletion date, obv. nullable
     */
    @Column(name = "deletion_date")
    private Date deletionDate;

    /**
     * Creation date (Hibernate)
     */
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
