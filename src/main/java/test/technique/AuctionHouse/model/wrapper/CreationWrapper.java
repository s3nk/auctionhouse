package test.technique.AuctionHouse.model.wrapper;

public class CreationWrapper<T> {
    private T objectCreated;
    private String location;

    public CreationWrapper() {
    }

    public CreationWrapper(T objectCreated, String location) {
        this.objectCreated = objectCreated;
        this.location = location;
    }

    public T getObjectCreated() {
        return objectCreated;
    }

    public void setObjectCreated(T objectCreated) {
        this.objectCreated = objectCreated;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
