package test.technique.AuctionHouse.model.rest;

import java.io.Serializable;

public class AuctionHouseGetRest implements Serializable {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Display name
     */
    private String name;

    public AuctionHouseGetRest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
