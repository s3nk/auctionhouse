package test.technique.AuctionHouse.model.rest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuctionHouseCreateRest {
    /**
     * Display name
     */
    @NotNull
    @Size(min = 1, max = 30)
    private String name;

    public AuctionHouseCreateRest() {
    }

    public AuctionHouseCreateRest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
