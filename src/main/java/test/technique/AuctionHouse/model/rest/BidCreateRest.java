package test.technique.AuctionHouse.model.rest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class BidCreateRest {
    /**
     * User token (identifier)
     */
    @NotNull
    @Size(min = 1, max = 128)
    private String userToken;

    /**
     * Amount
     */
    @Min(0)
    @NotNull
    private BigDecimal amount;

    public BidCreateRest() {
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
