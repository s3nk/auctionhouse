package test.technique.AuctionHouse.model.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BidGetRest implements Serializable {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Auction id
     */
    private Long auctionId;

    /**
     * User id
     */
    private Long userId;

    /**
     * User public name
     */
    private String userPublicName;

    /**
     * Amount
     */
    private BigDecimal amount;

    /**
     * Creation date
     */
    private Date creationDate;

    public BidGetRest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserPublicName() {
        return userPublicName;
    }

    public void setUserPublicName(String userPublicName) {
        this.userPublicName = userPublicName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
