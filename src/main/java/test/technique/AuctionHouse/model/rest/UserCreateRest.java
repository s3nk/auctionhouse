package test.technique.AuctionHouse.model.rest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserCreateRest {
    /**
     * User public name
     */
    @Size(min = 1, max = 128)
    @NotNull
    private String userPublicName;

    public UserCreateRest() {
    }

    public UserCreateRest(String userPublicName) {
        this.userPublicName = userPublicName;
    }

    public String getUserPublicName() {
        return userPublicName;
    }

    public void setUserPublicName(String userPublicName) {
        this.userPublicName = userPublicName;
    }
}
