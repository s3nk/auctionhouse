package test.technique.AuctionHouse.model.rest;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AuctionCreateRest {
    /**
     * Display name
     */
    @NotNull
    @Size(min = 1, max = 128)
    private String name;

    /**
     * Description
     */
    private String description;

    /**
     * Start date
     */
    @NotNull
    private LocalDateTime startDate;

    /**
     * End date
     */
    @NotNull
    private LocalDateTime endDate;

    /**
     * Start price
     */
    @NotNull
    @Min(0)
    private BigDecimal startPrice;

    public AuctionCreateRest() {
    }

    public AuctionCreateRest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }
}
