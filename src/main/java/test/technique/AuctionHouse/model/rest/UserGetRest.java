package test.technique.AuctionHouse.model.rest;

import java.io.Serializable;

public class UserGetRest implements Serializable {
    /**
     * Unique ID
     */
    private Long id;

    /**
     * Identification token (secret)
     */
    private String token;

    /**
     * Auction id
     */
    private Long auctionId;

    /**
     * User public name
     */
    private String userPublicName;

    public UserGetRest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Long auctionId) {
        this.auctionId = auctionId;
    }

    public String getUserPublicName() {
        return userPublicName;
    }

    public void setUserPublicName(String userPublicName) {
        this.userPublicName = userPublicName;
    }
}
