package test.technique.AuctionHouse.model.enumeration;

public enum AuctionStatus {
    NOT_STARTED,
    RUNNING,
    TERMINATED,
    DELETED,
    ALL
}
