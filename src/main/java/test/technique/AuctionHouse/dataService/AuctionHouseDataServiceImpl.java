package test.technique.AuctionHouse.dataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dao.AuctionHouseDao;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.AuctionHouseMapper;
import test.technique.AuctionHouse.model.entity.AuctionHouseEntity;
import test.technique.AuctionHouse.model.model.AuctionHouseModel;

import java.util.Date;
import java.util.Optional;

@Service
public class AuctionHouseDataServiceImpl implements AuctionHouseDataService {

    @Autowired
    private AuctionHouseDao auctionHouseDao;

    @Autowired
    private AuctionHouseMapper auctionHouseMapper;

    /**
     * Create an auction house
     * @param auctionHouse Object to create
     * @return Object created
     */
    @Override
    @CacheEvict(value = "auctionHousesLists", allEntries = true)
    public AuctionHouseModel create(AuctionHouseModel auctionHouse) {
        AuctionHouseEntity auctionHouseEntity = auctionHouseMapper.modelToEntity(auctionHouse);
        return auctionHouseMapper.entityToModel(auctionHouseDao.save(auctionHouseEntity));
    }

    /**
     * Get all the non-deleted auction houses
     * @return A list with the objects
     */
    @Override
    @Cacheable("auctionHousesLists")
    public Page<AuctionHouseModel> getAll(Pageable pageable) {
        Page<AuctionHouseEntity> entities = auctionHouseDao.findAllByDeletionDateIsNull(pageable);
        return entities.map(entity -> auctionHouseMapper.entityToModel(entity));
    }

    /**
     * Get an auction house by id
     * @param auctionHouseId Object id
     * @return The auction house object
     * @throws NotFoundException If there is no auction house with the given id a NotFoundException will occur
     */
    @Override
    @Cacheable("auctionHouses")
    public AuctionHouseModel get(Long auctionHouseId) throws NotFoundException {
        AuctionHouseEntity auctionHouseEntity = auctionHouseDao.findById(auctionHouseId).orElseThrow(() -> new NotFoundException(auctionHouseId, AuctionHouseEntity.class, String.format("Cannot find auction house with id %s", auctionHouseId)));
        return auctionHouseMapper.entityToModel(auctionHouseEntity);
    }

    /**
     * Get an potentiolly existing auction house by name
     * @param actionHouseName Object name
     * @return An optional with the auction house is it exists
     */
    @Override
    public Optional<AuctionHouseModel> getByName(String actionHouseName) {
        Optional<AuctionHouseEntity> oAuctionHouseEntity = auctionHouseDao.findByName(actionHouseName);
        return oAuctionHouseEntity.map(auctionHouseEntity -> auctionHouseMapper.entityToModel(auctionHouseEntity));
    }

    /**
     * Delete an auction house (Just update the deletion date if it was not specified)
     * @param auctionHouseId Object id
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "auctionHouses", allEntries = true),
            @CacheEvict(value = "auctionHousesLists", allEntries = true)
    })
    public void delete(Long auctionHouseId) {
        Optional<AuctionHouseEntity> oAuctionHouseEntity = auctionHouseDao.findById(auctionHouseId);
        if (oAuctionHouseEntity.isPresent() && oAuctionHouseEntity.get().getDeletionDate() == null) {
            AuctionHouseEntity auctionHouseEntity = oAuctionHouseEntity.get();
            auctionHouseEntity.setDeletionDate(new Date());
            auctionHouseDao.save(auctionHouseEntity);
        }
    }
}
