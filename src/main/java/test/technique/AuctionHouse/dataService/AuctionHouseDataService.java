package test.technique.AuctionHouse.dataService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.model.AuctionHouseModel;

import java.util.Optional;

public interface AuctionHouseDataService {
    AuctionHouseModel create(AuctionHouseModel auctionHouse);
    Page<AuctionHouseModel> getAll(Pageable pageable);
    AuctionHouseModel get(Long auctionHouseId) throws NotFoundException;
    Optional<AuctionHouseModel> getByName(String actionHouseName);
    void delete(Long auctionHouseId);
}
