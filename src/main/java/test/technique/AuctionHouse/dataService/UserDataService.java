package test.technique.AuctionHouse.dataService;

import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.model.UserModel;

import java.util.List;

public interface UserDataService {
    UserModel create(UserModel user);
    UserModel get(Long userId) throws NotFoundException;
    UserModel getByToken(String userToken) throws NotFoundException;
    List<UserModel> getAll(Long auctionId, List<Long> userIds);
}
