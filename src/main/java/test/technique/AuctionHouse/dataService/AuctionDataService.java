package test.technique.AuctionHouse.dataService;

import org.springframework.data.domain.Sort;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.model.AuctionModel;

import java.util.List;

public interface AuctionDataService {
    AuctionModel create(AuctionModel auction);
    List<AuctionModel> getAll(Long auctionHouseId, Sort sort);
    AuctionModel get(Long auctionId) throws NotFoundException;
    void delete(Long auctionId);
    void deleteByAuctionHouseId(Long auctionHouseId);
}
