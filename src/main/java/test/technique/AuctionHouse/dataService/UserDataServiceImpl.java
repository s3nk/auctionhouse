package test.technique.AuctionHouse.dataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dao.UserDao;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.UserMapper;
import test.technique.AuctionHouse.model.entity.UserEntity;
import test.technique.AuctionHouse.model.model.UserModel;

import java.util.List;

@Service
public class UserDataServiceImpl implements UserDataService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserMapper userMapper;

    /**
     * Create an User
     * @param user Object to create
     * @return Object created
     */
    @Override
    @CacheEvict(value = "usersLists", allEntries = true)
    public UserModel create(UserModel user) {
        UserEntity userEntity = userMapper.modelToEntity(user);
        return userMapper.entityToModel(userDao.save(userEntity));
    }

    /**
     * Get a User by id
     * @param userId Object id
     * @return The user object
     * @throws NotFoundException If there is no user with the given id a NotFoundException will occur
     */
    @Override
    @Cacheable("users")
    public UserModel get(Long userId) throws NotFoundException {
        UserEntity userEntity = userDao.findById(userId).orElseThrow(() -> new NotFoundException(userId, UserEntity.class, String.format("Cannot find user with id %s", userId)));
        return userMapper.entityToModel(userEntity);
    }

    /**
     * Get a User by token
     * @param userToken User token
     * @return The user object
     * @throws NotFoundException If there is no user with the given id a NotFoundException will occur
     */
    @Override
    @Cacheable("usersByTokens")
    public UserModel getByToken(String userToken) throws NotFoundException {
        UserEntity userEntity = userDao.findByToken(userToken).orElseThrow(() -> new NotFoundException(userToken, UserEntity.class, String.format("Cannot find user with token %s", userToken)));
        return userMapper.entityToModel(userEntity);
    }

    /**
     * Return all the users with the given ID
     * @param auctionId Auction id
     * @param userIds User ids
     * @return A list of users models
     */
    @Override
    @Cacheable("usersLists")
    public List<UserModel> getAll(Long auctionId, List<Long> userIds) {
        return userMapper.entitiesToModels(userDao.findByIdsAndAuctionId(auctionId, userIds));
    }
}
