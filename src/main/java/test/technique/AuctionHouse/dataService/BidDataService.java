package test.technique.AuctionHouse.dataService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.model.BidModel;

import java.util.List;
import java.util.Optional;

public interface BidDataService {
    BidModel create(BidModel bid);
    Page<BidModel> getAll(Long auctionId, Pageable pageable);
    BidModel getById(Long bidId) throws NotFoundException;
    Optional<BidModel> getBest(Long auctionId);
}
