package test.technique.AuctionHouse.dataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dao.BidDao;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.BidMapper;
import test.technique.AuctionHouse.model.entity.AuctionHouseEntity;
import test.technique.AuctionHouse.model.entity.BidEntity;
import test.technique.AuctionHouse.model.entity.UserEntity;
import test.technique.AuctionHouse.model.model.BidModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BidDataServiceImpl implements BidDataService {

    @Autowired
    private BidDao bidDao;

    @Autowired
    private BidMapper bidMapper;

    /**
     * Create a bid
     * @param bid Object to create
     * @return Object created
     */
    @Override
    @CacheEvict(value = "bidsLists", allEntries = true)
    public BidModel create(BidModel bid) {
        BidEntity bidEntity = bidMapper.modelToEntity(bid);
        return bidMapper.entityToModel(bidDao.save(bidEntity));
    }

    /**
     * Get all the auction bid
     * @param auctionId Auction id
     * @return A list with the objects
     */
    @Override
    @Cacheable("bidsLists")
    public Page<BidModel> getAll(Long auctionId, Pageable pageable) {
        return bidDao.findByAuctionId(auctionId, pageable).map(bid -> bidMapper.entityToModel(bid));
    }

    /**
     * Get a bid by id
     * @param bidId Object id
     * @return The bid object
     * @throws NotFoundException If there is no bid with the given id a NotFoundException will occur
     */
    @Override
    @Cacheable("bids")
    public BidModel getById(Long bidId) throws NotFoundException {
        BidEntity bidEntity = bidDao.findById(bidId).orElseThrow(() -> new NotFoundException(bidId, BidEntity.class, String.format("Cannot find bid with id %s", bidId)));
        return bidMapper.entityToModel(bidEntity);
    }

    /**
     * Get the best bid for the given auction
     * @param auctionId Auction id
     * @return The best bid if available, an empty Optional otherwise
     */
    @Override
    public Optional<BidModel> getBest(Long auctionId) {
        Page<BidEntity> entities = bidDao.findByAuctionId(auctionId, PageRequest.of(0, 10, new Sort(Sort.Direction.DESC, "amount")));
        return entities.stream()
                .filter(elem -> elem.getDeletionDate() == null)
                .max(Comparator.comparing(BidEntity::getAmount))
                .map(elem -> bidMapper.entityToModel(elem));
    }
}
