package test.technique.AuctionHouse.dataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dao.AuctionDao;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.AuctionMapper;
import test.technique.AuctionHouse.model.entity.AuctionEntity;
import test.technique.AuctionHouse.model.model.AuctionModel;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuctionDataServiceImpl implements AuctionDataService {
    @Autowired
    private AuctionDao auctionDao;

    @Autowired
    private AuctionMapper auctionMapper;

    /**
     * Create an auction
     * @param auction Object to create
     * @return Object created
     */
    @Override
    @CacheEvict(value = "auctionsLists", allEntries = true)
    public AuctionModel create(AuctionModel auction) {
        AuctionEntity auctionEntity = auctionMapper.modelToEntity(auction);
        return auctionMapper.entityToModel(auctionDao.save(auctionEntity));
    }

    /**
     * Get an auction by id
     * @param auctionId Object id
     * @return The auction object
     * @throws NotFoundException If there is no auction with the given id a NotFoundException will occur
     */
    @Override
    @Cacheable("auctions")
    public AuctionModel get(Long auctionId) throws NotFoundException {
        AuctionEntity auctionEntity = auctionDao.findById(auctionId).orElseThrow(() -> new NotFoundException(auctionId, AuctionEntity.class, String.format("Cannot find auction with id %s", auctionId)));
        return auctionMapper.entityToModel(auctionEntity);
    }

    /**
     * Get all the non-deleted auctions with the current status filter
     * @return A list with the objects
     */
    @Override
    @Cacheable("auctionsLists")
    public List<AuctionModel> getAll(Long auctionHouseId, Sort sort) {
        List<AuctionEntity> entities = auctionDao.findAll(sort);
        entities = entities.stream().filter(entity -> entity.getDeletionDate() == null).collect(Collectors.toList());
        return auctionMapper.entitiesToModels(entities);
    }

    /**
     * Delete an auction (Just update the deletion date if it was not specified)
     * @param auctionId Object id
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "auctions", allEntries = true),
            @CacheEvict(value = "auctionsLists", allEntries = true)
    })
    public void delete(Long auctionId) {
        Optional<AuctionEntity> oAuctionEntity = auctionDao.findById(auctionId);
        if (oAuctionEntity.isPresent() && oAuctionEntity.get().getDeletionDate() == null) {
            AuctionEntity auctionEntity = oAuctionEntity.get();
            auctionEntity.setDeletionDate(new Date());
            auctionDao.save(auctionEntity);
        }
    }

    /**
     * Delete all the auctions in the given auction house
     * @param auctionHouseId Auction house id
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "auctions", allEntries = true),
            @CacheEvict(value = "auctionsLists", allEntries = true)
    })
    public void deleteByAuctionHouseId(Long auctionHouseId) {
        List<AuctionEntity> auctionEntities = auctionDao.findByAuctionHouseId(auctionHouseId);
        boolean somethingChanged = false;
        for(AuctionEntity auctionEntity : auctionEntities) {
            if (auctionEntity.getDeletionDate() == null) {
                auctionEntity.setDeletionDate(new Date());
                somethingChanged = true;
            }
        }
        if (somethingChanged) {
            auctionDao.saveAll(auctionEntities);
        }
    }
}
