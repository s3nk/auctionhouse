package test.technique.AuctionHouse.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.exception.UnauthorizedException;
import test.technique.AuctionHouse.model.rest.*;
import test.technique.AuctionHouse.model.tools.ApiPageable;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;
import test.technique.AuctionHouse.service.AuctionService;
import test.technique.AuctionHouse.service.BidService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@Api(tags = {"Bid Resource"})
public class BidController {

    @Autowired
    private BidService bidService;

    @ApiOperation(value = "Create an bid")
    @PostMapping(value  = "v1/auctions/{auctionId}/bids")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created"),
            @ApiResponse(code = 400, message = "A problem was found in the request (more informations in the request body)"),
            @ApiResponse(code = 401, message = "The user is not authorized to bid in this auction")
    })
    public ResponseEntity<CreationWrapper<BidGetRest>> postBid(
            @ApiParam(value = "Auction to bid ID", required = true) @PathVariable Long auctionId,
            @ApiParam(value = "Bid data to add", required = true) @RequestBody @Valid BidCreateRest bid
    ) throws NotFoundException, UnauthorizedException, FunctionalException {
        BidGetRest getBidCreated = this.bidService.create(auctionId, bid);
        URI resourceLocation = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/v1/bids/{id}").buildAndExpand(getBidCreated.getId()).toUri();
        CreationWrapper<BidGetRest> wrapper = new CreationWrapper<>(getBidCreated, resourceLocation.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(resourceLocation);
        return new ResponseEntity<>(wrapper, headers, HttpStatus.CREATED);
    }
    
    @ApiOperation(value = "Return all the bids in the specified auction")
    @GetMapping(value  = "v1/auctions/{auctionId}/bids")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @ApiPageable
    public Page<BidGetRest> getBids(
            @ApiParam(value = "Parent auction ID", required = true) @PathVariable Long auctionId,
            Pageable pageable
    ) throws NotFoundException {
        return this.bidService.getAll(auctionId, pageable);
    }

    @ApiOperation(value = "Return the bid with the given identifier")
    @GetMapping(value  = "v1/bids/{bidId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved element"),
            @ApiResponse(code = 404, message = "The element was not found")
    })
    public BidGetRest getBid(
            @ApiParam(value = "Bid to get ID", required = true) @PathVariable Long bidId
    ) throws NotFoundException {
        return this.bidService.get(bidId);
    }

    @ApiOperation(value = "Return the best bid in the specified auction")
    @GetMapping(value  = "v1/auctions/{auctionId}/bids/BEST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted (or didn't exist)"),
            @ApiResponse(code = 404, message = "The auction was not found")
    })
    public BidGetRest getBestBid(
            @ApiParam(value = "Bid to delete ID", required = true) @PathVariable Long auctionId
    ) throws NotFoundException {
        return this.bidService.getBest(auctionId);
    }
}
