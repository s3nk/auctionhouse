package test.technique.AuctionHouse.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;
import test.technique.AuctionHouse.model.tools.ApiPageable;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;
import test.technique.AuctionHouse.service.AuctionHouseService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@Api(tags = {"Auction house Resource"})
public class AuctionHouseController {

    @Autowired
    private AuctionHouseService auctionHouseService;

    @ApiOperation(value = "Create an auction house")
    @PostMapping(value  = "v1/auctionHouses")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created"),
            @ApiResponse(code = 400, message = "A problem was found in the request (more informations in the request body)")
    })
    public ResponseEntity<CreationWrapper<AuctionHouseGetRest>> postAuctionHouse(
            @ApiParam(value = "Auction house to add", required = true) @RequestBody @Valid AuctionHouseCreateRest auctionHouse
    ) throws FunctionalException {
        AuctionHouseGetRest getAuctionHouseCreated = this.auctionHouseService.create(auctionHouse);
        URI resourceLocation = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(getAuctionHouseCreated.getId()).toUri();
        CreationWrapper<AuctionHouseGetRest> wrapper = new CreationWrapper<>(getAuctionHouseCreated, resourceLocation.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(resourceLocation);
        return new ResponseEntity<>(wrapper, headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Return all the auction houses")
    @GetMapping(value  = "v1/auctionHouses")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @ApiPageable
    public Page<AuctionHouseGetRest> getAuctionHouses(
            Pageable pageable
    ) {
        return this.auctionHouseService.getAll(pageable);
    }

    @ApiOperation(value = "Return an auction house with its ID")
    @GetMapping(value  = "v1/auctionHouses/{auctionHouseId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved element"),
            @ApiResponse(code = 404, message = "The element was not found")
    })
    public AuctionHouseGetRest getAuctionHouse(
            @ApiParam(value = "Auction house to get ID", required = true) @PathVariable Long auctionHouseId
    ) throws NotFoundException {
        return this.auctionHouseService.get(auctionHouseId);
    }

    @ApiOperation(value = "Delete an auction house with its ID")
    @DeleteMapping(value  = "v1/auctionHouses/{auctionHouseId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted (or didn't exist)"),
            @ApiResponse(code = 400, message = "A problem was found in the request (more informations in the request body)")
    })
    public ResponseEntity<Void> deleteAuctionHouse(
            @ApiParam(value = "Auction house to delete ID", required = true) @PathVariable Long auctionHouseId,
            @ApiParam(value = "If the deletion must be forced (if any auction is running in the auction house the deletion will only be possible if the 'force' option is specified)") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force
    ) throws FunctionalException {
        this.auctionHouseService.delete(auctionHouseId, force);
        return ResponseEntity.ok().build();
    }
}
