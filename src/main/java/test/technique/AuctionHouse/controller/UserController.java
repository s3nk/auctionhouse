package test.technique.AuctionHouse.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.rest.*;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;
import test.technique.AuctionHouse.service.BidService;
import test.technique.AuctionHouse.service.UserService;

import javax.validation.Valid;
import java.net.URI;

@RestController
@Api(tags = {"User Resource"})
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Create an user who can bid")
    @PostMapping(value  = "v1/auctions/{auctionId}/users")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created"),
            @ApiResponse(code = 404, message = "The auction was not found")
    })
    public ResponseEntity<CreationWrapper<UserGetRest>> postUser(
            @ApiParam(value = "Auction when the user will be able to bid", required = true) @PathVariable Long auctionId,
            @ApiParam(value = "User data to add", required = true) @RequestBody @Valid UserCreateRest user
    ) throws NotFoundException {
        UserGetRest getUserCreated = this.userService.create(auctionId, user);
        URI resourceLocation = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/v1/users/{id}").buildAndExpand(getUserCreated.getId()).toUri();
        CreationWrapper<UserGetRest> wrapper = new CreationWrapper<>(getUserCreated, resourceLocation.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(resourceLocation);
        return new ResponseEntity<>(wrapper, headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Return an user with its ID")
    @GetMapping(value  = "v1/users/{userId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved element"),
            @ApiResponse(code = 404, message = "The element was not found")
    })
    public UserGetRest getUser(
            @ApiParam(value = "User to get ID", required = true) @PathVariable Long userId
    ) throws NotFoundException {
        return this.userService.get(userId);
    }
}
