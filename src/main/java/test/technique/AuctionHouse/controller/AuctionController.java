package test.technique.AuctionHouse.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.rest.AuctionCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;
import test.technique.AuctionHouse.model.tools.ApiPageable;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;
import test.technique.AuctionHouse.service.AuctionHouseService;
import test.technique.AuctionHouse.service.AuctionService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;

@RestController
@Api(tags = {"Auction Resource"})
public class AuctionController {

    @Autowired
    private AuctionService auctionService;

    @ApiOperation(value = "Create an auction")
    @PostMapping(value  = "v1/auctionHouses/{houseId}/auctions")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created"),
            @ApiResponse(code = 404, message = "The auction was not found")
    })
    public ResponseEntity<CreationWrapper<AuctionGetRest>> postAuction(
            @ApiParam(value = "Parent auction house ID", required = true) @PathVariable Long houseId,
            @ApiParam(value = "Object to create", required = true) @RequestBody @Valid AuctionCreateRest auction
    ) throws NotFoundException {
        AuctionGetRest getAuctionCreated = this.auctionService.create(houseId, auction);
        URI resourceLocation = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/v1/auctions/{id}").buildAndExpand(getAuctionCreated.getId()).toUri();
        CreationWrapper<AuctionGetRest> wrapper = new CreationWrapper<>(getAuctionCreated, resourceLocation.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(resourceLocation);
        return new ResponseEntity<>(wrapper, headers, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Return all the auctions in a specified auction house")
    @GetMapping(value  = "v1/auctionHouses/{houseId}/auctions")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @ApiPageable
    public Page<AuctionGetRest> getAuctions(
            @ApiParam(value = "Parent auction house ID", required = true)  @PathVariable Long houseId,
            @ApiParam(value = "Auction state filter (optional)")  @RequestParam(value = "state", required = false, defaultValue = "ALL") AuctionStatus filterStatus,
            Pageable pageable
    ) {
        return this.auctionService.getAll(houseId, filterStatus, pageable);
    }

    @ApiOperation(value = "Return an auction with its ID")
    @GetMapping(value  = "v1/auctions/{auctionId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved element"),
            @ApiResponse(code = 404, message = "The element was not found")
    })
    public AuctionGetRest getAuction(
            @ApiParam(value = "Auction to get ID", required = true) @PathVariable Long auctionId
    ) throws NotFoundException {
        return this.auctionService.get(auctionId);
    }

    @ApiOperation(value = "Delete an auction with its ID")
    @DeleteMapping(value  = "v1/auctions/{auctionId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted (or didn't exist)")
    })
    public ResponseEntity<Void> deleteAuction(
            @ApiParam(value = "Auction to delete ID", required = true) @PathVariable Long auctionId
    ) {
        this.auctionService.delete(auctionId);
        return ResponseEntity.ok().build();
    }
}
