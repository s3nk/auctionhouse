package test.technique.AuctionHouse.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.exception.UnauthorizedException;
import test.technique.AuctionHouse.model.rest.BidCreateRest;
import test.technique.AuctionHouse.model.rest.BidGetRest;

import java.util.List;

public interface BidService {
    BidGetRest create(Long auctionId, BidCreateRest bid) throws NotFoundException, FunctionalException, UnauthorizedException;
    Page<BidGetRest> getAll(Long auctionId, Pageable pageable) throws NotFoundException;
    BidGetRest get(Long bidId) throws NotFoundException;
    BidGetRest getBest(Long auctionId) throws NotFoundException;
}
