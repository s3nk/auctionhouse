package test.technique.AuctionHouse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dataService.AuctionHouseDataService;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.AuctionHouseMapper;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.model.AuctionHouseModel;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;

import java.util.Optional;

@Service
public class AuctionHouseServiceImpl implements AuctionHouseService {
    private final Logger log = LoggerFactory.getLogger(AuctionHouseServiceImpl.class);

    @Autowired
    private AuctionHouseDataService auctionHouseDataService;

    @Autowired
    private AuctionService auctionService;

    @Autowired
    private AuctionHouseMapper auctionHouseMapper;

    /**
     * Create a new Auction House
     * @param auctionHouse Object to create
     * @return The object created
     * @throws FunctionalException The name muste be unique, if the given name is already used, an exception will occur
     */
    @Override
    public AuctionHouseGetRest create(AuctionHouseCreateRest auctionHouse) throws FunctionalException {
        log.info(String.format("Auction house creation request with name %s", auctionHouse.getName()));
        Optional<AuctionHouseModel> oExistingAuctionHouseModel = auctionHouseDataService.getByName(auctionHouse.getName());
        if (oExistingAuctionHouseModel.isPresent()) {
            log.error(String.format("Auction house creation request with name %s [failure] - already exists", auctionHouse.getName()));
            throw new FunctionalException(String.format("An auction house with the name %s already exists", auctionHouse.getName()), "alreadyExists");
        }
        AuctionHouseModel auctionHouseModel = auctionHouseMapper.createRestToModel(auctionHouse);
        AuctionHouseModel auctionHouseCreatedModel = auctionHouseDataService.create(auctionHouseModel);
        log.info(String.format("Auction house creation request with name %s [success] - id : %s", auctionHouse.getName(), auctionHouseCreatedModel.getId()));
        return auctionHouseMapper.modelToGetRest(auctionHouseCreatedModel);
    }

    /**
     * Get all the currently existing auction houses
     * @return A list with the auction houses
     */
    @Override
    public Page<AuctionHouseGetRest> getAll(Pageable pageable) {
        return auctionHouseDataService.getAll(pageable).map(auctionHouse -> auctionHouseMapper.modelToGetRest(auctionHouse));
    }

    /**
     * Get an auction house with its unique id
     * @param auctionHouseId Object id
     * @return The auction house
     * @throws NotFoundException If the auction house does not exist or was deleted a not found exception will occur
     */
    @Override
    public AuctionHouseGetRest get(Long auctionHouseId) throws NotFoundException {
        return auctionHouseMapper.modelToGetRest(auctionHouseDataService.get(auctionHouseId));
    }

    /**
     * Delete an auction house by id
     * @param auctionHouseId Object id
     * @param force If the deletion must be forced (if any auction is running on the auction house, it cannot deleted without 'force' option)
     * @throws FunctionalException If any auction is running on the auction house and the 'force option' was not specified, the error will occur
     */
    @Override
    public void delete(Long auctionHouseId, Boolean force) throws FunctionalException {
        log.info(String.format("Auction house deletion request %s", auctionHouseId));
        try {
            auctionHouseDataService.get(auctionHouseId);
            int auctionsRunningCount = auctionService.getAll(auctionHouseId, AuctionStatus.RUNNING, PageRequest.of(0, 10)).getContent().size();
            if (auctionsRunningCount > 0 && !force) {
                log.error(String.format("Auction house deletion request %s [failure] - %s auction(s) is/are running", auctionHouseId, auctionsRunningCount));
                throw new FunctionalException(String.format("Cannot delete auction house because %s auction(s) is/are running, retry with the 'force' parameter", auctionsRunningCount), "auctionsRunning");
            }
            auctionService.deleteByAuctionHouse(auctionHouseId);
            auctionHouseDataService.delete(auctionHouseId);
            log.info(String.format("Auction house deletion request %s [success]", auctionHouseId));
        } catch (NotFoundException e) {
            log.error(String.format("Auction house deletion request %s [failure] - does not exist", auctionHouseId));
        }
    }
}
