package test.technique.AuctionHouse.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;

public interface AuctionHouseService {
    AuctionHouseGetRest create(AuctionHouseCreateRest auctionHouse) throws FunctionalException;
    Page<AuctionHouseGetRest> getAll(Pageable pageable);
    AuctionHouseGetRest get(Long auctionHouseId) throws NotFoundException;
    void delete(Long auctionHouseId, Boolean force) throws FunctionalException;
}
