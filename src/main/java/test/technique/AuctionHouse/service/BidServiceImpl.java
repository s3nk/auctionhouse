package test.technique.AuctionHouse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dataService.AuctionDataService;
import test.technique.AuctionHouse.dataService.BidDataService;
import test.technique.AuctionHouse.dataService.UserDataService;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.exception.UnauthorizedException;
import test.technique.AuctionHouse.mapper.BidMapper;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.model.AuctionModel;
import test.technique.AuctionHouse.model.model.BidModel;
import test.technique.AuctionHouse.model.model.UserModel;
import test.technique.AuctionHouse.model.rest.BidCreateRest;
import test.technique.AuctionHouse.model.rest.BidGetRest;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BidServiceImpl implements BidService {
    private final Logger log = LoggerFactory.getLogger(BidServiceImpl.class);

    @Autowired
    private BidDataService bidDataService;

    @Autowired
    private AuctionDataService auctionDataService;

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private BidMapper bidMapper;

    /**
     * The status is not "Running", we must throw the right exception depending the dates
     *
     * @param auction Auction (to get the status)
     * @throws FunctionalException The exception depending the auction dates
     */
    private void throwErrorCannotBidANonRunndingAuction(AuctionModel auction) throws FunctionalException {
        switch (auction.getStatus()) {
            case NOT_STARTED:
                throw new FunctionalException(String.format("The auction %s has not started yet", auction.getName()), "NotStarted");
            case TERMINATED:
                throw new FunctionalException(String.format("The auction %s already ended", auction.getName()), "AlreadyEnded");
            case DELETED:
                throw new FunctionalException(String.format("The auction %s was deleted", auction.getName()), "Deleted");
        }
    }

    /**
     * Check if the auction is available to bid
     *
     * @param auctionId Auction id
     * @param bid       Bid content
     * @throws NotFoundException   If the auction was not found, can not bid on it
     * @throws FunctionalException If the auction is not running, can not bid on it
     */
    private void checkAuction(Long auctionId, BidCreateRest bid) throws FunctionalException, NotFoundException {
        try {
            AuctionModel auction = auctionDataService.get(auctionId);
            if (!auction.getStatus().equals(AuctionStatus.RUNNING)) {
                log.error(String.format("Bid creation request with price %s and user %s in the auction %s [failure] - status not eligible to bid %s", bid.getAmount(), bid.getUserToken(), auctionId, auction.getStatus()));
                throwErrorCannotBidANonRunndingAuction(auction);
            }
        } catch (NotFoundException e) {
            log.error(String.format("Bid creation request with price %s and user %s in the auction %s [failure] - auction does not exist", bid.getAmount(), bid.getUserToken(), auctionId));
            throw e;
        }
    }

    /**
     * Check if the bid amount allow its user to create it
     *
     * @param auctionId Auction id
     * @param bid       Bid content
     * @throws NotFoundException     If the user is not found, we will not create it
     * @throws UnauthorizedException If the user token was created in another auction he cannot bid
     */
    private UserModel checkUserRights(Long auctionId, BidCreateRest bid) throws UnauthorizedException, NotFoundException {
        try {
            UserModel user = userDataService.getByToken(bid.getUserToken());
            if (!user.getAuctionId().equals(auctionId)) {
                log.error(String.format("Bid creation request with price %s and user %s in the auction %s [failure] - the user %s was not created to bid in the auction %s", bid.getAmount(), bid.getUserToken(), auctionId, user.getId(), auctionId));
                throw new UnauthorizedException(String.format("The user %s is not authorized to bid in the auction %s", user.getId(), auctionId), "UserBadAuction");
            }
            return user;
        } catch (NotFoundException e) {
            log.error(String.format("Bid creation request with price %s and user %s in the auction %s [failure] - user or userToken does not exist", bid.getAmount(), bid.getUserToken(), auctionId));
            throw e;
        }
    }

    /**
     * Check if the bid amount allow its user to create it
     *
     * @param auctionId Auction id
     * @param bid       Bid content
     * @throws FunctionalException If the user bid amount is less or equals than the last bid, we will not create it
     */
    private void checkAmount(Long auctionId, BidCreateRest bid) throws FunctionalException {
        BigDecimal currentMax = bidDataService.getBest(auctionId).orElseGet(() -> {
            BidModel returnValue = new BidModel();
            returnValue.setAmount(BigDecimal.ZERO);
            return returnValue;
        }).getAmount();
        if (bid.getAmount().compareTo(currentMax) <= 0) {
            log.error(String.format("Bid creation request with price %s and user %s in the auction %s [failure] - amount is not larger than the previous best bid (previous amount : %s and new amount : %s)", bid.getAmount(), bid.getUserToken(), auctionId, currentMax, bid.getAmount()));
            throw new FunctionalException(String.format("The amount is not larger than the previous best bid (previous amount : %s and new amount : %s)", currentMax, bid.getAmount()), "AmountNotAcceptable");
        }
    }

    /**
     * Create a new Bid
     *
     * @param auctionId Auction
     * @param bid       Object to create
     * @return The object created
     */
    @Override
    public BidGetRest create(Long auctionId, BidCreateRest bid) throws NotFoundException, FunctionalException, UnauthorizedException {
        log.info(String.format("Bid creation request with price %s and user %s in the auction %s", bid.getAmount(), bid.getUserToken(), auctionId));
        // 1st - Check the auction
        checkAuction(auctionId, bid);

        // 2nd check the user and its rights in the auction
        UserModel user = checkUserRights(auctionId, bid);

        // 3rd check the amount
        checkAmount(auctionId, bid);

        // 4th all is ok, create the bid
        BidModel bidModel = bidMapper.createRestToModel(bid);
        bidModel.setAuctionId(auctionId);
        bidModel.setUserId(user.getId());
        BidModel bidCreatedModel = bidDataService.create(bidModel);
        log.info(String.format("Bid creation request with price %s and user %s in the auction %s [success] - id : %s", bid.getAmount(), bid.getUserToken(), auctionId, bidCreatedModel.getId()));
        BidGetRest bidGetRest = bidMapper.modelToGetRest(bidCreatedModel);
        bidGetRest.setUserPublicName(user.getUserPublicName()); // We want the user public name with the bid in http responses
        return bidGetRest;
    }

    /**
     * Get all the current bids in the auction
     *
     * @return A list with the bids
     */
    @Override
    public Page<BidGetRest> getAll(Long auctionId, Pageable pageable) throws NotFoundException {
        auctionDataService.get(auctionId);
        Page<BidModel> bidModels = bidDataService.getAll(auctionId, pageable);
        List<UserModel> userModels = userDataService.getAll(auctionId, bidModels.stream().map(BidModel::getUserId).collect(Collectors.toList()));
        return bidModels
                .map(bid -> bidMapper.modelToGetRest(bid))
                .map(bid -> {
                    bid.setUserPublicName(userModels.stream().filter(user -> user.getId().equals(bid.getUserId())).findFirst().map(UserModel::getUserPublicName).orElse(""));
                    return bid;
                });
    }

    /**
     * Get the bid with the specified id
     *
     * @param bidId Object id
     * @return The object
     * @throws NotFoundException If there is no object with the given id, a NotFoundException will be thrown
     */
    @Override
    public BidGetRest get(Long bidId) throws NotFoundException {
        BidModel bidModel = bidDataService.getById(bidId);
        BidGetRest bidGetRest = bidMapper.modelToGetRest(bidModel);
        try {
            UserModel user = userDataService.get(bidModel.getUserId());
            bidGetRest.setUserPublicName(user.getUserPublicName());
        } catch (NotFoundException e) { // The 404 error will not be thrown, just return the user name void to allow an individual get
            // This case is impossible.... while the db remains coherent (fk)
            log.error(String.format("User not found during bid get call with id %s - user %s", bidId, bidModel.getUserId()));
        }
        return bidGetRest;
    }

    /**
     * Return the best bid in the specified auction (if the auction exists and any bid exists)
     *
     * @param auctionId Auction unique id
     * @return The best bid if available
     * @throws NotFoundException If the auction does not exist or has no bid, a NotFoundException will be thrown
     */
    @Override
    public BidGetRest getBest(Long auctionId) throws NotFoundException {
        auctionDataService.get(auctionId);
        BidModel bidModel = bidDataService.getBest(auctionId).orElseThrow(() -> new NotFoundException(null, BidModel.class, String.format("Cannot find bid in the auction %s", auctionId)));
        BidGetRest bidGetRest = bidMapper.modelToGetRest(bidModel);
        try {
            UserModel user = userDataService.get(bidModel.getUserId());
            bidGetRest.setUserPublicName(user.getUserPublicName());
        } catch (NotFoundException e) { // The 404 error will not be thrown, just return the user name void to allow an individual get
            // This case is impossible.... while the db remains coherent (fk)
            log.error(String.format("User not found during best bid get call in auction %s - user %s", auctionId, bidModel.getUserId()));
        }
        return bidGetRest;
    }
}
