package test.technique.AuctionHouse.service;

import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;
import test.technique.AuctionHouse.model.rest.UserCreateRest;
import test.technique.AuctionHouse.model.rest.UserGetRest;

public interface UserService {
    UserGetRest create(Long auctionId, UserCreateRest user) throws NotFoundException;
    UserGetRest get(Long userId) throws NotFoundException;
}
