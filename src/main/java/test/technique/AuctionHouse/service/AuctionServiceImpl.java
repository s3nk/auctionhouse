package test.technique.AuctionHouse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dataService.AuctionDataService;
import test.technique.AuctionHouse.dataService.AuctionHouseDataService;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.AuctionMapper;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.model.AuctionModel;
import test.technique.AuctionHouse.model.rest.AuctionCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuctionServiceImpl implements AuctionService {
    private final Logger log = LoggerFactory.getLogger(AuctionServiceImpl.class);
    @Autowired
    private AuctionDataService auctionDataService;

    @Autowired
    private AuctionHouseDataService auctionHouseDataService;

    @Autowired
    private AuctionMapper auctionMapper;

    /**
     * Create a new Auction
     * @param auctionHouseId Parent auction house
     * @param auction Object to create
     * @return The object created
     */
    @Override
    public AuctionGetRest create(Long auctionHouseId, AuctionCreateRest auction) throws NotFoundException {
        log.info(String.format("Auction creation request with name %s in the auction house %s", auction.getName(), auctionHouseId));
        try {
            auctionHouseDataService.get(auctionHouseId);
        } catch (NotFoundException e) {
            log.error(String.format("Auction creation request with name %s in the auction house %s [failure] - auction house does not exist", auction.getName(), auctionHouseId));
            throw e;
        }
        AuctionModel auctionModel = auctionMapper.createRestToModel(auction);
        auctionModel.setAuctionHouseId(auctionHouseId);
        AuctionModel auctionCreatedModel = auctionDataService.create(auctionModel);
        log.info(String.format("Auction creation request with name %s in the auction house %s [success] - id : %s", auction.getName(), auctionHouseId, auctionCreatedModel.getId()));
        return auctionMapper.modelToGetRest(auctionCreatedModel);
    }

    /**
     * Get all the currently existing auctions in the given auction house
     * @param auctionHouseId Parent auction house
     * @param statusFilter Status filter (ALL, the default value doesn't filter)
     * @return A list with the auction houses
     */
    @Override
    public Page<AuctionGetRest> getAll(Long auctionHouseId, AuctionStatus statusFilter, Pageable pageable) {
        List<AuctionModel> models = auctionDataService.getAll(auctionHouseId, pageable.getSort());
        if (!statusFilter.equals(AuctionStatus.ALL)) {
            models = models.stream().filter(model -> model.getStatus().equals(statusFilter)).collect(Collectors.toList());
        }
        List<AuctionGetRest> restModels = auctionMapper.modelsToGetRest(models).subList(Math.min(pageable.getPageNumber() * pageable.getPageSize(), models.size()), Math.min((pageable.getPageNumber() + 1) * pageable.getPageSize(), models.size()));
        return new PageImpl<>(restModels, pageable, models.size());
    }

    /**
     * Get an auction with its unique id
     * @param auctionId Object id
     * @return The auction
     * @throws NotFoundException If the auction does not exist or was deleted a not found exception will occur
     */
    @Override
    public AuctionGetRest get(Long auctionId) throws NotFoundException {
        return auctionMapper.modelToGetRest(auctionDataService.get(auctionId));
    }

    /**
     * Delete an auction by id
     * @param auctionId Object id
     */
    @Override
    public void delete(Long auctionId) {
        log.info(String.format("Auction deletion request %s", auctionId));
        try {
            auctionDataService.get(auctionId);
            auctionDataService.delete(auctionId);
            log.info(String.format("Auction deletion request %s [success]", auctionId));
        } catch (NotFoundException e) {
            log.error(String.format("Auction deletion request %s [failure] - does not exist", auctionId));
        }
    }

    /**
     * Delete all auctions in the specified auction house
     * @param auctionHouseId Auction house id
     */
    @Override
    public void deleteByAuctionHouse(Long auctionHouseId) {
        log.info(String.format("Auction deletion request with the auction house %s", auctionHouseId));
        auctionDataService.deleteByAuctionHouseId(auctionHouseId);
        log.info(String.format("Auction deletion request with the auction house %s [success]", auctionHouseId));
    }
}
