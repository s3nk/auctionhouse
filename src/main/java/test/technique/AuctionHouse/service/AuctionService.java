package test.technique.AuctionHouse.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.rest.AuctionCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;

import java.util.List;

public interface AuctionService {
    AuctionGetRest create(Long auctionHouseId, AuctionCreateRest auction) throws NotFoundException;
    Page<AuctionGetRest> getAll(Long auctionHouseId, AuctionStatus statusFilter, Pageable pageable);
    AuctionGetRest get(Long auctionId) throws NotFoundException;
    void delete(Long auctionId);
    void deleteByAuctionHouse(Long auctionHouseId);
}
