package test.technique.AuctionHouse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.technique.AuctionHouse.dataService.AuctionDataService;
import test.technique.AuctionHouse.dataService.UserDataService;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.mapper.UserMapper;
import test.technique.AuctionHouse.model.model.UserModel;
import test.technique.AuctionHouse.model.rest.UserCreateRest;
import test.technique.AuctionHouse.model.rest.UserGetRest;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private AuctionDataService auctionDataService;

    @Autowired
    private UserMapper userMapper;

    /**
     * Create a new User
     * @param auctionId Auction where the user want to bid
     * @param user Object to create
     * @return The object created
     */
    @Override
    public UserGetRest create(Long auctionId, UserCreateRest user) throws NotFoundException {
        log.info(String.format("User creation request with name %s in the auction %s", user.getUserPublicName(), auctionId));
        try {
            auctionDataService.get(auctionId);
        } catch (NotFoundException e) {
            log.error(String.format("User creation request with name %s in the auction %s [failure] - auction does not exist", user.getUserPublicName(), auctionId));
            throw e;
        }
        UserModel userModel = userMapper.createRestToModel(user);
        userModel.setAuctionId(auctionId);
        userModel.setToken(UUID.randomUUID().toString());
        UserModel userCreatedModel = userDataService.create(userModel);
        log.info(String.format("User creation request with name %s in the auction %s [success] - id : %s", user.getUserPublicName(), auctionId, userCreatedModel.getId()));
        return userMapper.modelToGetRest(userCreatedModel);
    }

    /**
     * Get an user with its unique id
     * @param userId Object id
     * @return The user
     * @throws NotFoundException If the user does not exist or was deleted a not found exception will occur
     */
    @Override
    public UserGetRest get(Long userId) throws NotFoundException {
        return userMapper.modelToGetRest(userDataService.get(userId));
    }
}
