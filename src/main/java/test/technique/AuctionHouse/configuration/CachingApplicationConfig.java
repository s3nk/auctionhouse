package test.technique.AuctionHouse.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Cache manager able to deal with lists
 */
@Configuration
@EnableCaching
public class CachingApplicationConfig {
}
