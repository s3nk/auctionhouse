package test.technique.AuctionHouse.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("test.technique.AuctionHouse"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .tags(
                        new Tag("Auction house Resource", "Auction house management"),
                        new Tag("Auction Resource", "Auction management"),
                        new Tag("Bid Resource", "Bid management"),
                        new Tag("User Resource", "User management")
                );
    }
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Auction house API",
                "Auction house management API.",
                "1.0",
                "Terms of service",
                new Contact("Alexis", "", "wkg.lgt@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
