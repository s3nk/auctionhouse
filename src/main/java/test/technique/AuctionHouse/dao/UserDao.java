package test.technique.AuctionHouse.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import test.technique.AuctionHouse.model.entity.AuctionHouseEntity;
import test.technique.AuctionHouse.model.entity.BidEntity;
import test.technique.AuctionHouse.model.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserDao extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByToken(String token);

    @Query(value = "select usr from UserEntity as usr where usr.auctionId = :auctionId and usr.id in (:userIds)")
    List<UserEntity> findByIdsAndAuctionId(@Param("auctionId") Long auctionId, @Param("userIds") List<Long> userIds);
}
