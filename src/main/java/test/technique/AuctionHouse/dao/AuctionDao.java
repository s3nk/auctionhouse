package test.technique.AuctionHouse.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import test.technique.AuctionHouse.model.entity.AuctionEntity;

import java.util.List;

public interface AuctionDao  extends JpaRepository<AuctionEntity, Long> {
    List<AuctionEntity> findByAuctionHouseId(Long auctionHouseId);
}
