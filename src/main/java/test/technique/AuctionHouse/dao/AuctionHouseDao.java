package test.technique.AuctionHouse.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import test.technique.AuctionHouse.model.entity.AuctionEntity;
import test.technique.AuctionHouse.model.entity.AuctionHouseEntity;

import java.util.List;
import java.util.Optional;

public interface AuctionHouseDao extends JpaRepository<AuctionHouseEntity, Long> {
    Optional<AuctionHouseEntity> findByName(String name);
    Page<AuctionHouseEntity> findAllByDeletionDateIsNull(Pageable pageable);
}
