package test.technique.AuctionHouse.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import test.technique.AuctionHouse.model.entity.BidEntity;

public interface BidDao extends JpaRepository<BidEntity, Long> {
    Page<BidEntity> findByAuctionId(Long auctionId, Pageable pageable);
}
