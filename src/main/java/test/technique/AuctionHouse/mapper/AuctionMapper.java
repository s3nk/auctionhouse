package test.technique.AuctionHouse.mapper;

import org.mapstruct.Mapper;
import test.technique.AuctionHouse.model.entity.AuctionEntity;
import test.technique.AuctionHouse.model.model.AuctionModel;
import test.technique.AuctionHouse.model.rest.AuctionCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface AuctionMapper {
    AuctionModel createRestToModel(AuctionCreateRest auctionCreateRest);

    AuctionGetRest modelToGetRest(AuctionModel auctionModel);
    default List<AuctionGetRest> modelsToGetRest(List<AuctionModel> auctionModels) {
        return auctionModels.stream().map(this::modelToGetRest).collect(Collectors.toList());
    }

    AuctionEntity modelToEntity(AuctionModel auctionModel);
    AuctionModel entityToModel(AuctionEntity auctionEntity);

    default List<AuctionModel> entitiesToModels(List<AuctionEntity> auctionEntities) {
        return auctionEntities.stream().map(this::entityToModel).collect(Collectors.toList());
    }
}
