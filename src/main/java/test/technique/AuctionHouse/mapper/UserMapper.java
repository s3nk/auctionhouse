package test.technique.AuctionHouse.mapper;

import org.mapstruct.Mapper;
import test.technique.AuctionHouse.model.entity.UserEntity;
import test.technique.AuctionHouse.model.model.UserModel;
import test.technique.AuctionHouse.model.rest.UserCreateRest;
import test.technique.AuctionHouse.model.rest.UserGetRest;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserModel createRestToModel(UserCreateRest userCreateRest);
    UserGetRest modelToGetRest(UserModel userModel);
    UserEntity modelToEntity(UserModel userModel);
    UserModel entityToModel(UserEntity userEntity);
    default List<UserModel> entitiesToModels(List<UserEntity> userEntities) {
        return userEntities.stream().map(this::entityToModel).collect(Collectors.toList());
    }
}
