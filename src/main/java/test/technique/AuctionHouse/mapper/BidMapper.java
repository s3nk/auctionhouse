package test.technique.AuctionHouse.mapper;

import org.mapstruct.Mapper;
import test.technique.AuctionHouse.model.entity.BidEntity;
import test.technique.AuctionHouse.model.model.BidModel;
import test.technique.AuctionHouse.model.rest.BidCreateRest;
import test.technique.AuctionHouse.model.rest.BidGetRest;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface BidMapper {
    BidModel createRestToModel(BidCreateRest bidCreateRest);

    BidGetRest modelToGetRest(BidModel bidModel);

    BidEntity modelToEntity(BidModel bidModel);
    BidModel entityToModel(BidEntity bidEntity);
}
