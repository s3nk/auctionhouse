package test.technique.AuctionHouse.mapper;

import org.mapstruct.Mapper;
import test.technique.AuctionHouse.model.entity.AuctionHouseEntity;
import test.technique.AuctionHouse.model.model.AuctionHouseModel;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;

@Mapper(componentModel = "spring")
public interface AuctionHouseMapper {
    AuctionHouseModel createRestToModel(AuctionHouseCreateRest auctionHouseCreateRest);

    AuctionHouseGetRest modelToGetRest(AuctionHouseModel auctionHouseModel);

    AuctionHouseEntity modelToEntity(AuctionHouseModel auctionHouseModel);
    AuctionHouseModel entityToModel(AuctionHouseEntity auctionHouseEntity);
}
