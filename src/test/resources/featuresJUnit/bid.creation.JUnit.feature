Feature: Creation d'une offre - unitaire

  Scenario: Creation d'une nouvelle offre sur une enchere inexistante
    Given Mocked - L'enchere 10 n'existe pas
    When Mocked - Je cree une offre de "10" pour l'utilisateur ayant pour token "Bobby" sur l'enchere 10
    Then Mocked - La creation de l'offre de "10" est un echec avec une erreur 404

  Scenario: Creation d'une nouvelle offre avec un token associe a personne
    Given Mocked - L'enchere 10 existe sur la maison 42
    And Mocked - L'utilisateur ayant pour token "Bobby" n'existe pas
    When Mocked - Je cree une offre de "10" pour l'utilisateur ayant pour token "Bobby" sur l'enchere 10
    Then Mocked - La creation de l'offre de "10" est un echec avec une erreur 404