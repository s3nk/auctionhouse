Feature: Récuperation des offres - unitaire

  Scenario: Recuperation de la meilleure offre dont l'utilisateur n'existe plus
    Given Mocked - L'enchere 10 existe sur la maison 42
    And Mocked - La meilleure offre de l'enchere 10 est de "100" proposee par l'utilisateur 1
    And Mocked - L'utilisateur 1 n'existe pas
    When Mocked - Je recupere la meilleure offre de l'enchere 10
    Then Mocked - La recuperation de l'offre de "100" de l'utilisateur 1 est un succes mais elle ne contient pas de nom d'utilisateur

  Scenario: Recuperation d'une offre unitaire dont l'utilisateur n'existe plus
    Given Mocked - L'enchere 10 existe sur la maison 42
    And Mocked - L'offre 666 de l'enchere 10 est de "100" par l'utilisateur 1
    And Mocked - L'utilisateur 1 n'existe pas
    When Mocked - Je recupere l'offre 666
    Then Mocked - La recuperation de l'offre de "100" de l'utilisateur 1 est un succes mais elle ne contient pas de nom d'utilisateur