Feature: Creation d'une enchère
    @needCacheEviction
    Scenario: Creation d'un nouvel utilisateur sur une enchere qui existe
        Given La maison d'identifiant 42 existe
        And L'enchere "testEnchere" n'existe pas
        When Je cree l'enchere "testEnchere" sur la maison 42
        Then La creation de l'enchere "testEnchere" est un succes
        When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
        Then La creation de l'utilisateur "Bobby" est un succes
        # Clear
         When Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
         And Je supprime reellement l'enchere "testEnchere" sur la maison 42
         And Je supprime reellement la maison 42

    @needCacheEviction
    Scenario: Creation d'un nouvel utilisateur sur une enchere qui n'existe pas
        Given L'enchere 666 n'existe pas
        When Je cree l'utilisateur "Bobby" sur l'enchere 666
        Then La creation de l'utilisateur "Bobby" est un echec avec une erreur 404