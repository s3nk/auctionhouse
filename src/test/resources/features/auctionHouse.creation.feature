Feature: Creation d'une maison d'enchère

  @needCacheEviction
  Scenario: Creation d'une nouvelle maison
    Given La maison "testAuctionHouse" n'existe pas
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
    When Je cree la maison "testAuctionHouse"
    Then La creation de la maison "testAuctionHouse" est un succes
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouse" apparait dans les maisons recuperees
        # Clear
    When Je supprime reellement la maison "testAuctionHouse"

  @needCacheEviction
  Scenario: Creation d'une nouvelle maison avec un nom invalide
    Given La maison "testAuctionHouseMaisNomInvalideCarTropLong" n'existe pas
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouseMaisNomInvalideCarTropLong" n'apparait pas dans les maisons recuperees
    When Je cree la maison "testAuctionHouseMaisNomInvalideCarTropLong"
    Then La creation de la maison "testAuctionHouseMaisNomInvalideCarTropLong" est un echec
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouseMaisNomInvalideCarTropLong" n'apparait pas dans les maisons recuperees

  @needCacheEviction
  Scenario: Creation d'une nouvelle maison dupliquee
    Given La maison "testAuctionHouse" n'existe pas
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
    When Je cree la maison "testAuctionHouse"
    Then La creation de la maison "testAuctionHouse" est un succes
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouse" apparait dans les maisons recuperees
    When Je cree la maison "testAuctionHouse"
    Then La creation de la maison "testAuctionHouse" est un echec
    When Je recupere toutes les maisons
    Then La maison "testAuctionHouse" apparait dans les maisons recuperees
        # Clear
    When Je supprime reellement la maison "testAuctionHouse"