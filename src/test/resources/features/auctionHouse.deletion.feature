Feature: Suppression d'une maison d'enchère
    @needCacheEviction
    Scenario: Suppression d'une maison existante sans enchere
        Given La maison "testAuctionHouse" n'existe pas
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
        When Je cree la maison "testAuctionHouse"
        Then La creation de la maison "testAuctionHouse" est un succes
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" apparait dans les maisons recuperees
        When Je supprime la maison "testAuctionHouse"
        Then La suppression de la maison est un succes
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
        # Clear
        When Je supprime reellement la maison "testAuctionHouse"

    @needCacheEviction
    Scenario: Suppression d'une maison existante avec enchere
        Given La maison "testAuctionHouse" n'existe pas
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
        When Je cree la maison "testAuctionHouse"
        Then La creation de la maison "testAuctionHouse" est un succes
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" apparait dans les maisons recuperees
        When Je cree l'enchere "testEnchere" sur la maison "testAuctionHouse"
        Then La creation de l'enchere "testEnchere" est un succes
        When Je supprime la maison "testAuctionHouse"
        Then La suppression de la maison est un echec
        When Je supprime la maison "testAuctionHouse" en forcant
        Then La suppression de la maison est un succes
        When Je recupere toutes les maisons
        Then La maison "testAuctionHouse" n'apparait pas dans les maisons recuperees
        # Clear
        When Je supprime reellement l'enchere "testEnchere" sur la maison "testAuctionHouse"
        And Je supprime reellement la maison "testAuctionHouse"

    @needCacheEviction
    Scenario: Suppression d'une maison non existante
        Given La maison d'identifiant 666 n'existe pas
        When Je supprime la maison d'identifiant 666
        Then La suppression de la maison est un succes