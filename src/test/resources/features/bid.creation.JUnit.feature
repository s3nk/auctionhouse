Feature: Creation d'une offre - tests unitaires

  Scenario: Creation d'une nouvelle offre sur une enchere qui n'existe pas
    Given La maison d'identifiant 42 existe
    And L'enchere "testEnchere" n'existe pas
    When Je cree l'enchere "testEnchere" sur la maison 42
    Then La creation de l'enchere "testEnchere" est un succes
    When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'utilisateur "Bobby" est un succes
    When Je cree l'utilisateur "BobbyBis" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'utilisateur "BobbyBis" est un succes
    When Je cree une offre de "10" pour l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'offre de "10" est un succes
    When Je cree une offre de "9" pour l'utilisateur "BobbyBis" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'offre est un echec
    When Je cree une offre de "10" pour l'utilisateur "BobbyBis" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'offre est un echec
    When Je cree une offre de "11" pour l'utilisateur "BobbyBis" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'offre de "11" est un succes
    # Clear
    When Je supprime reellement les offres sur l'enchere "testEnchere" sur la maison 42
    And Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
    And Je supprime reellement l'utilisateur "BobbyBis" sur l'enchere "testEnchere" de la maison 42
    And Je supprime reellement l'enchere "testEnchere" sur la maison 42
    And Je supprime reellement la maison 42

  @needCacheEviction
  Scenario: Creation d'une nouvelle offre sur un utilisateur qui existe sur une enchere qui existe, mais pas la bonne
    Given La maison d'identifiant 42 existe
    And L'enchere "testEnchere" n'existe pas
    And L'enchere "testOtherEnchere" n'existe pas
    When Je cree l'enchere "testEnchere" sur la maison 42
    Then La creation de l'enchere "testEnchere" est un succes
    When Je cree l'enchere "testOtherEnchere" sur la maison 42
    Then La creation de l'enchere "testOtherEnchere" est un succes
    When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
    Then La creation de l'utilisateur "Bobby" est un succes
         # The next "when" contains the test, the error is thrown too early to catch it with the holder
    When Je cree une offre de "10" pour l'utilisateur "Bobby" sur l'enchere "testOtherEnchere" de la maison 42 - erreur volontaire enchere reelle "testEnchere"
         # Clear
    When Je supprime reellement les offres sur l'enchere "testEnchere" sur la maison 42
    And Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchere" de la maison 42
    And Je supprime reellement l'enchere "testEnchere" sur la maison 42
    And Je supprime reellement l'enchere "testOtherEnchere" sur la maison 42
    And Je supprime reellement la maison 42

  @needCacheEviction
  Scenario: Creation d'une nouvelle offre sur un utilisateur qui existe sur une enchere qui existe mais sont a des etats non-souscriptibles
    Given La maison d'identifiant 42 existe
    And L'enchere "testEnchere" n'existe pas
        # Not started status
    When Je cree l'enchere future "testEnchereNotStarted" sur la maison 42
    Then La creation de l'enchere "testEnchereNotStarted" est un succes
    When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchereNotStarted" de la maison 42
    Then La creation de l'utilisateur "Bobby" est un succes
    When Je cree une offre de "10" pour l'utilisateur "Bobby" sur l'enchere "testEnchereNotStarted" de la maison 42
    Then La creation de l'offre est un echec
        # Ended status
    When Je cree l'enchere terminee "testEnchereTerminated" sur la maison 42
    Then La creation de l'enchere "testEnchereTerminated" est un succes
    When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchereTerminated" de la maison 42
    Then La creation de l'utilisateur "Bobby" est un succes
    When Je cree une offre de "10" pour l'utilisateur "Bobby" sur l'enchere "testEnchereTerminated" de la maison 42
    Then La creation de l'offre est un echec
        # Deleted status
    When Je cree l'enchere "testEnchereDeleted" sur la maison 42
    Then La creation de l'enchere "testEnchereDeleted" est un succes
    When Je supprime l'enchere "testEnchereDeleted" sur la maison 42
    Then La suppression de l'enchere est un succes
    When Je cree l'utilisateur "Bobby" sur l'enchere "testEnchereDeleted" de la maison 42
    Then La creation de l'utilisateur "Bobby" est un succes
    When Je cree une offre de "10" pour l'utilisateur "Bobby" sur l'enchere "testEnchereDeleted" de la maison 42
    Then La creation de l'offre est un echec
        # Clear
    When Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchereTerminated" de la maison 42
    And Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchereDeleted" de la maison 42
    And Je supprime reellement l'utilisateur "Bobby" sur l'enchere "testEnchereNotStarted" de la maison 42
    And Je supprime reellement l'enchere "testEnchereTerminated" sur la maison 42
    And Je supprime reellement l'enchere "testEnchereDeleted" sur la maison 42
    And Je supprime reellement l'enchere "testEnchereNotStarted" sur la maison 42
    And Je supprime reellement la maison 42