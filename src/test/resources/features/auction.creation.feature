Feature: Creation d'une enchère
    @needCacheEviction
    Scenario: Creation d'une nouvelle enchère sur une maison qui existe
        Given La maison d'identifiant 42 existe
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchere" n'apparait pas dans les encheres recuperees
        When Je cree l'enchere "testEnchere" sur la maison 42
        Then La creation de l'enchere "testEnchere" est un succes
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchere" apparait dans les encheres recuperees
        # Clear
        When Je supprime reellement l'enchere "testEnchere" sur la maison 42
        When Je supprime reellement la maison 42

    @needCacheEviction
    Scenario: Creation d'enchère sur une maison qui existe pour check leurs statuts
        Given La maison d'identifiant 42 existe
        When Je cree l'enchere "testEnchereRunning" sur la maison 42
        Then La creation de l'enchere "testEnchereRunning" est un succes
        When Je cree l'enchere future "testEnchereNotStarted" sur la maison 42
        Then La creation de l'enchere "testEnchereNotStarted" est un succes
        When Je cree l'enchere terminee "testEnchereTerminated" sur la maison 42
        Then La creation de l'enchere "testEnchereTerminated" est un succes
        When Je cree l'enchere "testEnchereDeleted" sur la maison 42
        Then La creation de l'enchere "testEnchereDeleted" est un succes
        When Je supprime l'enchere "testEnchereDeleted" sur la maison 42
        Then La suppression de l'enchere est un succes
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchereRunning" apparait dans les encheres recuperees
        Then L'enchere "testEnchereNotStarted" apparait dans les encheres recuperees
        Then L'enchere "testEnchereTerminated" apparait dans les encheres recuperees
        Then L'enchere "testEnchereDeleted" n'apparait pas dans les encheres recuperees
        When Je recupere toutes les encheres en cours sur la maison 42
        Then L'enchere "testEnchereRunning" apparait dans les encheres recuperees
        Then L'enchere "testEnchereNotStarted" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereTerminated" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereDeleted" n'apparait pas dans les encheres recuperees
        When Je recupere toutes les encheres terminees sur la maison 42
        Then L'enchere "testEnchereRunning" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereNotStarted" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereTerminated" apparait dans les encheres recuperees
        Then L'enchere "testEnchereDeleted" n'apparait pas dans les encheres recuperees
        # The deleted objects must not be returned by the API
        When Je recupere toutes les encheres supprimees sur la maison 42
        Then L'enchere "testEnchereRunning" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereNotStarted" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereTerminated" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereDeleted" n'apparait pas dans les encheres recuperees
        When Je recupere toutes les encheres a venir sur la maison 42
        Then L'enchere "testEnchereRunning" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereNotStarted" apparait dans les encheres recuperees
        Then L'enchere "testEnchereTerminated" n'apparait pas dans les encheres recuperees
        Then L'enchere "testEnchereDeleted" n'apparait pas dans les encheres recuperees
        # Clear
        When Je supprime reellement l'enchere "testEnchereDeleted" sur la maison 42
        And Je supprime reellement l'enchere "testEnchereRunning" sur la maison 42
        And Je supprime reellement l'enchere "testEnchereNotStarted" sur la maison 42
        And Je supprime reellement l'enchere "testEnchereTerminated" sur la maison 42
        And Je supprime reellement la maison 42

    @needCacheEviction
    Scenario: Creation d'une nouvelle enchère sur une maison qui n'existe pas
        Given La maison d'identifiant 42 n'existe pas
        When Je recupere toutes les encheres sur la maison 42
        Then Une erreur 404 se produit lors de la recuperation des encheres
        When Je cree l'enchere "testEnchere" sur la maison 42
        Then La creation de l'enchere "testEnchere" est un echec avec une erreur 404
        When Je recupere toutes les encheres sur la maison 42
        Then Une erreur 404 se produit lors de la recuperation des encheres