Feature: Suppression d'une enchère
    @needCacheEviction
    Scenario: Suppression d'une nouvelle enchère
        Given La maison d'identifiant 42 existe
        And L'enchere "testEnchere" n'existe pas
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchere" n'apparait pas dans les encheres recuperees
        When Je cree l'enchere "testEnchere" sur la maison 42
        Then La creation de l'enchere "testEnchere" est un succes
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchere" apparait dans les encheres recuperees
        When Je supprime l'enchere "testEnchere" sur la maison 42
        Then La suppression de l'enchere est un succes
        When Je recupere toutes les encheres sur la maison 42
        Then L'enchere "testEnchere" n'apparait pas dans les encheres recuperees
        # Clear
        When Je supprime reellement l'enchere "testEnchere" sur la maison 42
        And Je supprime reellement la maison 42

    @needCacheEviction
    Scenario: Suppression d'une enchère qui n'exste pas
        Given La maison d'identifiant 42 existe
        And L'enchere 666 n'existe pas
        When Je supprime l'enchere 666
        Then La suppression de l'enchere est un succes
        # Clear
        When Je supprime reellement la maison 42