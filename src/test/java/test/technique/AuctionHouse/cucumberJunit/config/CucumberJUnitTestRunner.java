package test.technique.AuctionHouse.cucumberJunit.config;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"test.technique.AuctionHouse.cucumberJunit"}, features = "classpath:featuresJUnit", plugin = {"pretty", "json:target/cucumber-junit-report.json"})
@ActiveProfiles("test")
@ContextConfiguration
public class CucumberJUnitTestRunner {
}
