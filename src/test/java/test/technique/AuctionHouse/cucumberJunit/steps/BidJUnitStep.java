package test.technique.AuctionHouse.cucumberJunit.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.test.util.ReflectionTestUtils;
import test.technique.AuctionHouse.dataService.AuctionDataServiceImpl;
import test.technique.AuctionHouse.dataService.BidDataServiceImpl;
import test.technique.AuctionHouse.dataService.UserDataServiceImpl;
import test.technique.AuctionHouse.exception.FunctionalException;
import test.technique.AuctionHouse.exception.NotFoundException;
import test.technique.AuctionHouse.exception.UnauthorizedException;
import test.technique.AuctionHouse.mapper.BidMapper;
import test.technique.AuctionHouse.mapper.BidMapperImpl;
import test.technique.AuctionHouse.model.entity.AuctionEntity;
import test.technique.AuctionHouse.model.entity.UserEntity;
import test.technique.AuctionHouse.model.model.AuctionModel;
import test.technique.AuctionHouse.model.model.BidModel;
import test.technique.AuctionHouse.model.rest.BidCreateRest;
import test.technique.AuctionHouse.model.rest.BidGetRest;
import test.technique.AuctionHouse.service.BidServiceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

public class BidJUnitStep {

    @Mock
    public BidServiceImpl bidService;

    @Mock
    private AuctionDataServiceImpl auctionDataService;

    @Mock
    private UserDataServiceImpl userDataService;

    @Mock
    private BidDataServiceImpl bidDataService;

    private BidMapper bidMapper;

    private BidGetRest objectCreated;
    private Exception creationEception;

    private BidGetRest bidRead;
    private Exception bidReadEception;

    public BidJUnitStep() {
        MockitoAnnotations.initMocks(this);
        bidMapper = Mockito.mock(BidMapperImpl.class);
        ReflectionTestUtils.setField(bidService, "auctionDataService", auctionDataService);
        ReflectionTestUtils.setField(bidService, "userDataService", userDataService);
        ReflectionTestUtils.setField(bidService, "bidDataService", bidDataService);
        ReflectionTestUtils.setField(bidService, "bidMapper", bidMapper);
        Logger log = Mockito.mock(Logger.class);
        ReflectionTestUtils.setField(bidService, "log", log);
        Mockito.doCallRealMethod().when(bidMapper).modelToGetRest(any());
    }

    @Given("Mocked - L'enchere {long} n'existe pas")
    public void mockHouseDoesNotExist(Long auctionId) throws NotFoundException {
        Mockito.doThrow(new NotFoundException(auctionId, AuctionEntity.class, String.format("Cannot find auction with id %s", auctionId))).when(auctionDataService).get(auctionId);
    }

    @Given("Mocked - L'enchere {long} existe sur la maison {long}")
    public void mockHouseDoesExist(Long auctionId, Long auctionHouseId) throws NotFoundException {
        AuctionModel auction = new AuctionModel();
        auction.setId(auctionId);
        auction.setAuctionHouseId(auctionHouseId);
        auction.setStartDate(LocalDateTime.now().minusDays(1));
        auction.setEndDate(LocalDateTime.now().plusDays(1));
        Mockito.doReturn(auction).when(auctionDataService).get(auctionId);
    }

    @Given("Mocked - L'utilisateur ayant pour token {string} n'existe pas")
    public void userDoesNotExistByToken(String userToken) throws NotFoundException {
        Mockito.doThrow(new NotFoundException(userToken, UserEntity.class, String.format("Cannot find user with token %s", userToken))).when(userDataService).getByToken(userToken);
    }

    @Given("Mocked - La meilleure offre de l'enchere {long} est de {string} proposee par l'utilisateur {long}")
    public void bestBidrDefinition(Long auctionId, String amount, Long userId) {
        BidModel bidModel = new BidModel();
        bidModel.setUserId(userId);
        bidModel.setAmount(new BigDecimal(amount));
        bidModel.setAuctionId(auctionId);
        Mockito.doReturn(Optional.of(bidModel)).when(bidDataService).getBest(auctionId);
    }

    @Given("Mocked - L'offre {long} de l'enchere {long} est de {string} par l'utilisateur {long}")
    public void bidDefinition(Long bidId, Long auctionId, String amount, Long userId) throws NotFoundException {
        BidModel bidModel = new BidModel();
        bidModel.setUserId(userId);
        bidModel.setAmount(new BigDecimal(amount));
        bidModel.setAuctionId(auctionId);
        Mockito.doReturn(bidModel).when(bidDataService).getById(bidId);
    }

    @Given("Mocked - L'utilisateur {long} n'existe pas")
    public void userDoesNotExistById(Long userId) throws NotFoundException {
        Mockito.doThrow(new NotFoundException(userId, UserEntity.class, String.format("Cannot find user with id %s", userId))).when(userDataService).get(userId);
    }

    @When("Mocked - Je cree une offre de {string} pour l'utilisateur ayant pour token {string} sur l'enchere {long}")
    public void mockHouseDoesNotExist(String amount, String saverToken, Long auctionId) throws UnauthorizedException, NotFoundException, FunctionalException {
        Mockito.doCallRealMethod().when(bidService).create(any(), any());
        BidCreateRest bidToCreate = new BidCreateRest();
        bidToCreate.setAmount(new BigDecimal(amount));
        bidToCreate.setUserToken(saverToken);
        objectCreated = null;
        creationEception = null;
        try {
            objectCreated = bidService.create(auctionId, bidToCreate);
        } catch (Exception e) {
            creationEception = e;
        }
    }

    @Then("Mocked - La creation de l'offre de {string} est un echec avec une erreur 404")
    public void mockHouseDoesNotExist(String amount) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(objectCreated).isNull();
        softly.assertThat(creationEception instanceof NotFoundException).isTrue();
        softly.assertAll();
    }

    @When("Mocked - Je recupere la meilleure offre de l'enchere {long}")
    public void getAuctionBestBid(Long auctionId) throws NotFoundException {
        Mockito.doCallRealMethod().when(bidService).getBest(auctionId);
        bidRead = null;
        bidReadEception = null;
        try {
            bidRead = bidService.getBest(auctionId);
        } catch (Exception e) {
            bidReadEception = e;
        }
    }

    @When("Mocked - Je recupere l'offre {long}")
    public void getBidById(Long bidId) throws NotFoundException {
        Mockito.doCallRealMethod().when(bidService).get(bidId);
        bidRead = null;
        bidReadEception = null;
        try {
            bidRead = bidService.get(bidId);
        } catch (Exception e) {
            bidReadEception = e;
        }
    }

    @Then("Mocked - La recuperation de l'offre de {string} de l'utilisateur {long} est un succes mais elle ne contient pas de nom d'utilisateur")
    public void bidReadValidButNoUsername(String amount, Long userId) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(bidReadEception).isNull();
        softly.assertThat(bidRead).isNotNull();
        softly.assertThat(bidRead.getAmount().compareTo(new BigDecimal(amount))).isEqualTo(0);
        softly.assertThat(bidRead.getUserId()).isEqualTo(userId);
        softly.assertThat(bidRead.getUserPublicName()).isNull();
        softly.assertAll();
    }
}
