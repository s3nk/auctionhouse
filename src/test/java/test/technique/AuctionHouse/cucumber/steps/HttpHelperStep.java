package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;

public class HttpHelperStep {
    protected TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    protected HolderHttp holderHttp;

    @LocalServerPort
    protected int port;

    @Autowired
    protected ObjectMapper objectMapper;

    protected <T> void callApi(String path, HttpMethod method, T body) {
        ResponseEntity<?> response = restTemplate.exchange(
                "http://localhost:" + port + "/" + path, method,
                new HttpEntity<>(body, holderHttp.getHeaders()), String.class);
        holderHttp.setStatusCode(response.getStatusCode());
        holderHttp.setHeaders(response.getHeaders());
        if (response.hasBody()) {
            holderHttp.setBody((String) response.getBody());
        }
    }

    protected <T> void callApiWithFullPath(String fullPath, HttpMethod method, T body) {
        ResponseEntity<?> response = restTemplate.exchange(
                fullPath, method,
                new HttpEntity<>(body, holderHttp.getHeaders()), String.class);
        holderHttp.setStatusCode(response.getStatusCode());
        holderHttp.setHeaders(response.getHeaders());
        if (response.hasBody()) {
            holderHttp.setBody((String) response.getBody());
        }
    }
}
