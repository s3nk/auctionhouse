package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.web.client.ResourceAccessException;
import test.technique.AuctionHouse.cucumber.model.BidCheckModel;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;
import test.technique.AuctionHouse.model.rest.BidCreateRest;
import test.technique.AuctionHouse.model.rest.BidGetRest;
import test.technique.AuctionHouse.model.tools.RestPageImpl;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class BidStep extends HttpHelperStep {
    @Autowired
    private JdbcOperations jdbcOperations;

    private HolderHttp creationResult;

    private List<BidGetRest> bidsRead;

    private BidGetRest bidRead;

    @When("Je supprime reellement les offres sur l'enchere {string} sur la maison {long}")
    public void deleteBidsInDb(String auctionName, Long auctionHouseId) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        jdbcOperations.update("delete from bid where auction_id = ?", auctionId);
    }

    @When("Je cree une offre de {string} pour l'utilisateur {string} sur l'enchere {string} de la maison {long}")
    public void createBid(String amount, String userName, String auctionName, Long auctionHouseId) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        String userToken = jdbcOperations.queryForObject("select token from user where user_public_name = ? and auction_id = ?", new Object[]{ userName, auctionId }, (rs, rowNum) -> rs.getString("token"));
        BidCreateRest bidCreateRest = new BidCreateRest();
        bidCreateRest.setAmount(new BigDecimal(amount));
        bidCreateRest.setUserToken(userToken);
        callApi(String.format("/v1/auctions/%s/bids", auctionId), HttpMethod.POST, bidCreateRest);
        creationResult = holderHttp;
    }

    @When("Je cree une offre de {string} pour l'utilisateur {string} sur l'enchere {string} de la maison {long} - erreur volontaire enchere reelle {string}")
    public void createBid(String amount, String userName, String auctionName, Long auctionHouseId, String realAuctionName) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        Long realAuctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ realAuctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        String userToken = jdbcOperations.queryForObject("select token from user where user_public_name = ? and auction_id = ?", new Object[]{ userName, realAuctionId }, (rs, rowNum) -> rs.getString("token"));
        BidCreateRest bidCreateRest = new BidCreateRest();
        bidCreateRest.setAmount(new BigDecimal(amount));
        bidCreateRest.setUserToken(userToken);
        try {
            callApi(String.format("/v1/auctions/%s/bids", auctionId), HttpMethod.POST, bidCreateRest);
            Assert.fail();
        } catch (ResourceAccessException error) {
            Assert.assertNotNull(error);
        }
    }

    @Then("La creation de l'offre de {string} est un succes")
    public void bidCreationSuccess(String amount) throws IOException {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        CreationWrapper<BidCreateRest> result = objectMapper.readValue(holderHttp.getBody(), new TypeReference<CreationWrapper<BidCreateRest>>() {});
        softly.assertThat(result).isNotNull();
        softly.assertThat(result.getObjectCreated()).isNotNull();
        softly.assertThat(result.getObjectCreated().getAmount()).isEqualTo(new BigDecimal(amount));
        softly.assertAll();
    }

    @Then("La creation de l'offre est un echec")
    public void bidCreationFailureNotFound() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @When("Je recupere les offres sur l'enchere {string} de la maison {long}")
    public void readAllBids(String auctionName, Long auctionHouseId) throws IOException {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/auctions/%s/bids", auctionId), HttpMethod.GET, null);
        RestPageImpl<BidGetRest> page = objectMapper.readValue(holderHttp.getBody(), new TypeReference<RestPageImpl<BidGetRest>>() {});
        bidsRead = page.getContent();
    }

    @Then("Les offres sont bien")
    public void checkBids(DataTable dataTable) {
        List<BidCheckModel> bidToCheck = dataTable.asList(BidCheckModel.class);
        SoftAssertions softly = new SoftAssertions();
        bidToCheck.forEach(bid -> softly.assertThat(bidsRead.stream().anyMatch(readBid -> readBid.getUserPublicName().equals(bid.getUserName()) && (readBid.getAmount().compareTo(bid.getAmount()) == 0))).isTrue());
        softly.assertAll();
    }

    @When("Je recupere la meilleure offre de l'enchere {string} de la maison {long}")
    public void readBestBid(String auctionName, Long auctionHouseId) throws IOException {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/auctions/%s/bids/BEST", auctionId), HttpMethod.GET, null);
        bidRead = objectMapper.readValue(holderHttp.getBody(), BidGetRest.class);
    }

    @Then("L'offre recuperee est bien celle de {string} qui propose {string}")
    public void checkBids(String userName, String amount) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(bidRead).isNotNull();
        softly.assertThat(bidRead.getUserPublicName()).isEqualTo(userName);
        softly.assertThat(bidRead.getAmount().compareTo(new BigDecimal(amount))).isEqualTo(0);
        softly.assertAll();
    }

    @When("Je recupere la derniere offre de l'enchere {string} de la maison {long}")
    public void readLastBid(String auctionName, Long auctionHouseId) throws IOException {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        Long lastBidId = jdbcOperations.queryForObject("select id from bid where auction_id = ? order by creation_date desc limit 1", new Object[]{ auctionId }, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/bids/%s", lastBidId), HttpMethod.GET, null);
        bidRead = objectMapper.readValue(holderHttp.getBody(), BidGetRest.class);
    }
}
