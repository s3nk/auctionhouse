package test.technique.AuctionHouse.cucumber.steps;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import io.cucumber.datatable.DataTableType;
import io.cucumber.datatable.TableEntryTransformer;
import test.technique.AuctionHouse.cucumber.model.BidCheckModel;

import java.math.BigDecimal;
import java.util.Locale;

public class CustomTypeRegistryConfigurer implements TypeRegistryConfigurer {

    @Override
    public void configureTypeRegistry(TypeRegistry registry) {
        // Configure the translation from DataTable element to BidCheckModel
        registry.defineDataTableType(new DataTableType(BidCheckModel.class, (TableEntryTransformer<BidCheckModel>) entry -> new BidCheckModel(entry.get("userName"), new BigDecimal(entry.get("amount")))));
    }

    @Override
    public Locale locale() {
        return Locale.ENGLISH;
    }

}
