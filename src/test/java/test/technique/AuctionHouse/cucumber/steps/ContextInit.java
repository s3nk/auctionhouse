package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcOperations;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;

import java.io.IOException;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ContextInit {
}
