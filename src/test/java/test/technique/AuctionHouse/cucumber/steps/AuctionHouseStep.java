package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcOperations;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;
import test.technique.AuctionHouse.model.rest.AuctionHouseCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionHouseGetRest;
import test.technique.AuctionHouse.model.tools.RestPageImpl;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;

import java.io.IOException;
import java.util.List;

public class AuctionHouseStep extends HttpHelperStep {
    @Autowired
    private JdbcOperations jdbcOperations;

    private List<AuctionHouseGetRest> auctionHouses;
    private HolderHttp creationResult;
    private HolderHttp deletionResult;

    @Given("La maison {string} n'existe pas")
    public void initTestWithoutAuctionHouse(String auctionHouseName) {
        jdbcOperations.update("delete from auction_house where name = ?", auctionHouseName);
    }

    @Given("La maison d'identifiant {long} existe")
    public void initTestWithAuctionHouse(Long auctionHouseId) {
        jdbcOperations.update("delete from auction_house where id = ?", auctionHouseId);
        jdbcOperations.update("insert into auction_house (id, name) VALUES (?, ?)", auctionHouseId, "TestAuctionHouse" + auctionHouseId);
    }

    @Given("La maison d'identifiant {long} n'existe pas")
    public void initTestWithoutAuctionHouse(Long auctionHouseId) {
        jdbcOperations.update("delete from auction_house where id = ?", auctionHouseId);
    }

    @When("Je supprime reellement la maison {long}")
    public void deleteAuctionHouseInDb(Long auctionHouseId) {
        jdbcOperations.update("delete from auction_house where id = ?", auctionHouseId);
    }

    @When("Je supprime reellement la maison {string}")
    public void deleteAuctionHouseInDbByName(String auctionHouseName) {
        jdbcOperations.update("delete from auction_house where name = ?", auctionHouseName);
    }

    @When("Je recupere toutes les maisons")
    public void getAuctionHouse() throws IOException {
        callApi("/v1/auctionHouses", HttpMethod.GET, null);
        RestPageImpl<AuctionHouseGetRest> elementsRead = objectMapper.readValue(holderHttp.getBody(), new TypeReference<RestPageImpl<AuctionHouseGetRest>>() {
        });
        auctionHouses = elementsRead.getContent();
    }

    @When("Je cree la maison {string}")
    public void createAuctionHouse(String auctionHouseName) {
        AuctionHouseCreateRest auctionHouseCreateRest = new AuctionHouseCreateRest(auctionHouseName);
        callApi("/v1/auctionHouses", HttpMethod.POST, auctionHouseCreateRest);
        creationResult = holderHttp;
    }

    @When("Je supprime la maison {string}")
    public void deleteAuctionHouse(String auctionHouseName) {
        AuctionHouseCreateRest auctionHouseCreateRest = new AuctionHouseCreateRest(auctionHouseName);
        Long auctionHouseId = jdbcOperations.queryForObject("select id from auction_house where name = ?", new Object[]{auctionHouseName}, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/auctionHouses/%s", auctionHouseId), HttpMethod.DELETE, null);
        deletionResult = holderHttp;
    }

    @When("Je supprime la maison {string} en forcant")
    public void deleteAuctionHouseWithForce(String auctionHouseName) {
        AuctionHouseCreateRest auctionHouseCreateRest = new AuctionHouseCreateRest(auctionHouseName);
        Long auctionHouseId = jdbcOperations.queryForObject("select id from auction_house where name = ?", new Object[]{auctionHouseName}, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/auctionHouses/%s?force=true", auctionHouseId), HttpMethod.DELETE, null);
        deletionResult = holderHttp;
    }

    @When("Je supprime la maison d'identifiant {long}")
    public void deleteAuctionHouseById(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s", auctionHouseId), HttpMethod.DELETE, null);
        deletionResult = holderHttp;
    }

    @Then("La creation de la maison {string} est un succes")
    public void auctionHouseCreationSuccess(String houseName) throws IOException {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        CreationWrapper<AuctionHouseGetRest> result = objectMapper.readValue(holderHttp.getBody(), new TypeReference<CreationWrapper<AuctionHouseGetRest>>() {
        });
        softly.assertThat(result).isNotNull();
        softly.assertThat(result.getObjectCreated()).isNotNull();
        softly.assertThat(result.getObjectCreated().getName()).isEqualTo(houseName);
        softly.assertThat(result.getObjectCreated().getId()).isNotNull();
        softly.assertAll();

        callApiWithFullPath(creationResult.getHeaders().getLocation().toString(), HttpMethod.GET, null);
        AuctionHouseGetRest elementGotAfterCreation = objectMapper.readValue(holderHttp.getBody(), AuctionHouseGetRest.class);
        softly = new SoftAssertions();
        softly.assertThat(elementGotAfterCreation).isNotNull();
        softly.assertThat(elementGotAfterCreation.getId()).isEqualTo(result.getObjectCreated().getId());
        softly.assertThat(elementGotAfterCreation.getName()).isEqualTo(houseName);
        softly.assertAll();
    }

    @Then("La creation de la maison {string} est un echec")
    public void auctionHouseCreationFailure(String houseName) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Then("La suppression de la maison est un succes")
    public void auctionHouseDeletionSuccess() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(deletionResult).isNotNull();
        softly.assertThat(deletionResult.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Then("La suppression de la maison est un echec")
    public void auctionHouseDeletionFailure() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(deletionResult).isNotNull();
        softly.assertThat(deletionResult.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Then("La maison {string} n'apparait pas dans les maisons recuperees")
    public void checkAuctionHouseDoesntExist(String auctionHouseName) {
        Assert.assertTrue(auctionHouses.stream().noneMatch(auctionHouse -> auctionHouse.getName().equals(auctionHouseName)));
    }

    @Then("La maison {string} apparait dans les maisons recuperees")
    public void checkAuctionHouseExists(String auctionHouseName) {
        Assert.assertTrue(auctionHouses.stream().anyMatch(auctionHouse -> auctionHouse.getName().equals(auctionHouseName)));
    }
}
