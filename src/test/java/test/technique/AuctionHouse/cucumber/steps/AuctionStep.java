package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcOperations;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;
import test.technique.AuctionHouse.model.enumeration.AuctionStatus;
import test.technique.AuctionHouse.model.rest.AuctionCreateRest;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;
import test.technique.AuctionHouse.model.tools.RestPageImpl;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class AuctionStep extends HttpHelperStep {
    @Autowired
    private JdbcOperations jdbcOperations;

    private HolderHttp getResult;
    private HolderHttp creationResult;
    private HolderHttp deletionResult;

    @Given("L'enchere {long} n'existe pas")
    public void initTestWithoutAuction(Long auctionId) {
        jdbcOperations.update("delete from auction where id = ?", auctionId);
    }

    @Given("L'enchere {string} n'existe pas")
    public void initTestWithoutAuctionByName(String auctionName) {
        jdbcOperations.update("delete from auction where name = ?", auctionName);
    }

    @When("Je supprime reellement l'enchere {string} sur la maison {long}")
    public void deleteAuctionInDb(String auctionName, Long auctionHouseId) {
        jdbcOperations.update("delete from auction where name = ? and auction_house_id = ?", auctionName, auctionHouseId);
    }

    @When("Je supprime reellement l'enchere {string} sur la maison {string}")
    public void deleteAuctionInDbWithHouseName(String auctionName, String auctionHouseName) {
        Long auctionHouseId = jdbcOperations.queryForObject("select id from auction_house where name = ?", new Object[]{auctionHouseName}, (rs, rowNum) -> rs.getLong("id"));
        jdbcOperations.update("delete from auction where name = ? and auction_house_id = ?", auctionName, auctionHouseId);
    }

    @When("Je recupere toutes les encheres sur la maison {long}")
    public void getAuctions(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s/auctions", auctionHouseId), HttpMethod.GET, null);
        getResult = holderHttp;
    }

    @When("Je recupere toutes les encheres en cours sur la maison {long}")
    public void getRunningAuctions(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s/auctions?state=" + AuctionStatus.RUNNING, auctionHouseId), HttpMethod.GET, null);
        getResult = holderHttp;
    }

    @When("Je recupere toutes les encheres terminees sur la maison {long}")
    public void getEndedAuctions(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s/auctions?state=" + AuctionStatus.TERMINATED, auctionHouseId), HttpMethod.GET, null);
        getResult = holderHttp;
    }

    @When("Je recupere toutes les encheres supprimees sur la maison {long}")
    public void getDeletedAuctions(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s/auctions?state=" + AuctionStatus.DELETED, auctionHouseId), HttpMethod.GET, null);
        getResult = holderHttp;
    }

    @When("Je recupere toutes les encheres a venir sur la maison {long}")
    public void getFutureAuctions(Long auctionHouseId) {
        callApi(String.format("/v1/auctionHouses/%s/auctions?state=" + AuctionStatus.NOT_STARTED, auctionHouseId), HttpMethod.GET, null);
        getResult = holderHttp;
    }

    @Then("L'enchere {string} apparait dans les encheres recuperees")
    public void auctionListReadContainsAuctionName(String auctionName) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(getResult).isNotNull();
        softly.assertThat(getResult.getStatusCode()).isEqualTo(HttpStatus.OK);
        try {
            RestPageImpl<AuctionGetRest> pagedResult = objectMapper.readValue(getResult.getBody(), new TypeReference<RestPageImpl<AuctionGetRest>>() {
            });
            List<AuctionGetRest> auctions = pagedResult.getContent();
            softly.assertThat(auctions).isNotNull();
            softly.assertThat(auctions.stream().anyMatch(auctionHouse -> auctionHouse.getName().equals(auctionName))).isTrue();
        } catch (IOException e) {
            e.printStackTrace();
        }
        softly.assertAll();
    }

    @Then("L'enchere {string} n'apparait pas dans les encheres recuperees")
    public void auctionListReadDoesntContainAuctionName(String auctionName) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(getResult).isNotNull();
        softly.assertThat(getResult.getStatusCode()).isEqualTo(HttpStatus.OK);
        try {
            RestPageImpl<AuctionGetRest> page = objectMapper.readValue(getResult.getBody(), new TypeReference<RestPageImpl<AuctionGetRest>>() {
            });
            List<AuctionGetRest> auctions = page.getContent();
            softly.assertThat(auctions).isNotNull();
            softly.assertThat(auctions.stream().noneMatch(auctionHouse -> auctionHouse.getName().equals(auctionName))).isTrue();
        } catch (IOException e) {
            e.printStackTrace();
        }
        softly.assertAll();
    }

    @Then("Une erreur 404 se produit lors de la recuperation des encheres")
    public void auctionListReadNotFoundError() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(getResult).isNotNull();
        softly.assertThat(getResult.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @When("Je cree l'enchere {string} sur la maison {long}")
    public void createRunningAuction(String auctionName, Long auctionHouseId) {
        AuctionCreateRest auctionCreateRest = new AuctionCreateRest(auctionName);
        auctionCreateRest.setStartPrice(BigDecimal.ZERO);
        auctionCreateRest.setStartDate(LocalDateTime.now());
        auctionCreateRest.setEndDate(LocalDateTime.now().plusDays(1));
        callApi(String.format("/v1/auctionHouses/%s/auctions", auctionHouseId), HttpMethod.POST, auctionCreateRest);
        creationResult = holderHttp;
    }

    @When("Je cree l'enchere future {string} sur la maison {long}")
    public void createFutureAuction(String auctionName, Long auctionHouseId) {
        AuctionCreateRest auctionCreateRest = new AuctionCreateRest(auctionName);
        auctionCreateRest.setStartPrice(BigDecimal.ZERO);
        auctionCreateRest.setStartDate(LocalDateTime.now().plusDays(1));
        auctionCreateRest.setEndDate(LocalDateTime.now().plusDays(2));
        callApi(String.format("/v1/auctionHouses/%s/auctions", auctionHouseId), HttpMethod.POST, auctionCreateRest);
        creationResult = holderHttp;
    }

    @When("Je cree l'enchere terminee {string} sur la maison {long}")
    public void createEndedAuction(String auctionName, Long auctionHouseId) {
        AuctionCreateRest auctionCreateRest = new AuctionCreateRest(auctionName);
        auctionCreateRest.setStartPrice(BigDecimal.ZERO);
        auctionCreateRest.setStartDate(LocalDateTime.now().minusDays(2));
        auctionCreateRest.setEndDate(LocalDateTime.now().minusDays(1));
        callApi(String.format("/v1/auctionHouses/%s/auctions", auctionHouseId), HttpMethod.POST, auctionCreateRest);
        creationResult = holderHttp;
    }

    @When("Je cree l'enchere {string} sur la maison {string}")
    public void createAuctionByAuctionHouseName(String auctionName, String auctionHouseName) {
        Long auctionHouseId = jdbcOperations.queryForObject("select id from auction_house where name = ?", new Object[]{auctionHouseName}, (rs, rowNum) -> rs.getLong("id"));
        AuctionCreateRest auctionCreateRest = new AuctionCreateRest(auctionName);
        auctionCreateRest.setStartPrice(BigDecimal.ZERO);
        auctionCreateRest.setStartDate(LocalDateTime.now());
        auctionCreateRest.setEndDate(LocalDateTime.now().plusDays(1));
        callApi(String.format("/v1/auctionHouses/%s/auctions", auctionHouseId), HttpMethod.POST, auctionCreateRest);
        creationResult = holderHttp;
    }

    @Then("La creation de l'enchere {string} est un succes")
    public void auctionCreationSuccess(String auctionName) throws IOException {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        CreationWrapper<AuctionGetRest> result = objectMapper.readValue(holderHttp.getBody(), new TypeReference<CreationWrapper<AuctionGetRest>>() {
        });
        softly.assertThat(result).isNotNull();
        softly.assertThat(result.getObjectCreated()).isNotNull();
        softly.assertThat(result.getObjectCreated().getName()).isEqualTo(auctionName);
        softly.assertThat(result.getObjectCreated().getId()).isNotNull();
        softly.assertAll();

        callApiWithFullPath(creationResult.getHeaders().getLocation().toString(), HttpMethod.GET, null);
        AuctionGetRest elementGotAfterCreation = objectMapper.readValue(holderHttp.getBody(), AuctionGetRest.class);
        softly = new SoftAssertions();
        softly.assertThat(elementGotAfterCreation).isNotNull();
        softly.assertThat(elementGotAfterCreation.getId()).isEqualTo(result.getObjectCreated().getId());
        softly.assertThat(elementGotAfterCreation.getName()).isEqualTo(auctionName);
        softly.assertThat(elementGotAfterCreation.getAuctionHouseId()).isNotNull();
        softly.assertThat(elementGotAfterCreation.getEndDate()).isNotNull();
        softly.assertThat(elementGotAfterCreation.getStartDate()).isNotNull();
        softly.assertAll();
    }

    @Then("La creation de l'enchere {string} est un echec avec une erreur 404")
    public void auctionCreationFailureNotFound(String auctionName) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @When("Je supprime l'enchere {string} sur la maison {long}")
    public void deleteAuctionByNameAndAuctionHouseId(String auctionName, Long auctionHouseId) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{auctionName, auctionHouseId}, (rs, rowNum) -> rs.getLong("id"));
        callApi(String.format("/v1/auctions/%s", auctionId), HttpMethod.DELETE, null);
        deletionResult = holderHttp;
    }

    @When("Je supprime l'enchere {long}")
    public void deleteAuctionById(Long auctionId) {
        callApi(String.format("/v1/auctions/%s", auctionId), HttpMethod.DELETE, null);
        deletionResult = holderHttp;
    }

    @Then("La suppression de l'enchere est un succes")
    public void auctionHouseDeletionSuccess() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(deletionResult).isNotNull();
        softly.assertThat(deletionResult.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
