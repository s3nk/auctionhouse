package test.technique.AuctionHouse.cucumber.steps;

import cucumber.api.java.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;

public class CacheStep extends HttpHelperStep {
    @Autowired
    private CacheManager cacheManager;

    @Before("@needCacheEviction")
    public void evictAllCaches(){
        System.out.println("Cache clearing started");
        for(String name : cacheManager.getCacheNames()){
            System.out.println("Cache cleared - " + name);
            cacheManager.getCache(name).clear();
        }
    }
}
