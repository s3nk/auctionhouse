package test.technique.AuctionHouse.cucumber.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcOperations;
import test.technique.AuctionHouse.cucumber.tools.HolderHttp;
import test.technique.AuctionHouse.model.rest.AuctionGetRest;
import test.technique.AuctionHouse.model.rest.UserCreateRest;
import test.technique.AuctionHouse.model.rest.UserGetRest;
import test.technique.AuctionHouse.model.wrapper.CreationWrapper;

import java.io.IOException;

public class UserStep extends HttpHelperStep {
    @Autowired
    private JdbcOperations jdbcOperations;

    private HolderHttp creationResult;

    @When("Je supprime reellement l'utilisateur {string} sur l'enchere {string} de la maison {long}")
    public void deleteBidsInDb(String userName, String auctionName, Long auctionHouseId) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        jdbcOperations.update("delete from user where auction_id = ? and user_public_name = ?", auctionId, userName);
    }

    @When("Je cree l'utilisateur {string} sur l'enchere {string} de la maison {long}")
    public void createUser(String userName, String auctionName, Long auctionHouseId) {
        Long auctionId = jdbcOperations.queryForObject("select id from auction where name = ? and auction_house_id = ?", new Object[]{ auctionName, auctionHouseId }, (rs, rowNum) -> rs.getLong("id"));
        UserCreateRest userCreateRest = new UserCreateRest(userName);
        callApi(String.format("/v1/auctions/%s/users", auctionId), HttpMethod.POST, userCreateRest);
        creationResult = holderHttp;
    }

    @When("Je cree l'utilisateur {string} sur l'enchere {long}")
    public void createUser(String userName, Long auctionId) {
        UserCreateRest userCreateRest = new UserCreateRest(userName);
        callApi(String.format("/v1/auctions/%s/users", auctionId), HttpMethod.POST, userCreateRest);
        creationResult = holderHttp;
    }

    @Then("La creation de l'utilisateur {string} est un succes")
    public void userCreationSuccess(String userName) throws IOException {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        CreationWrapper<UserCreateRest> result = objectMapper.readValue(holderHttp.getBody(), new TypeReference<CreationWrapper<UserCreateRest>>() {});
        softly.assertThat(result).isNotNull();
        softly.assertThat(result.getObjectCreated()).isNotNull();
        softly.assertThat(result.getObjectCreated().getUserPublicName()).isEqualTo(userName);
        softly.assertAll();

        callApiWithFullPath(creationResult.getHeaders().getLocation().toString(), HttpMethod.GET, null);
        UserGetRest elementGotAfterCreation = objectMapper.readValue(holderHttp.getBody(), UserGetRest.class);
        softly = new SoftAssertions();
        softly.assertThat(elementGotAfterCreation).isNotNull();
        softly.assertThat(elementGotAfterCreation.getId()).isNotNull();
        softly.assertThat(elementGotAfterCreation.getUserPublicName()).isEqualTo(userName);
        softly.assertAll();
    }

    @Then("La creation de l'utilisateur {string} est un echec avec une erreur 404")
    public void userCreationFailureNotFound(String userName) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(creationResult).isNotNull();
        softly.assertThat(creationResult.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
