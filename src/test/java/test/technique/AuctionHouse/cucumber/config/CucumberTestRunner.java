package test.technique.AuctionHouse.cucumber.config;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import test.technique.AuctionHouse.AuctionHouseApplication;

@RunWith(Cucumber.class)
@CucumberOptions(glue = { "test.technique.AuctionHouse.cucumber" }, features = "classpath:features", plugin = {"pretty", "json:target/cucumber-report.json"})
@ActiveProfiles("test")
@ContextConfiguration
public class CucumberTestRunner {
}
