package test.technique.AuctionHouse.cucumber.model;

import java.math.BigDecimal;

public class BidCheckModel {
    private String userName;
    private BigDecimal amount;

    public BidCheckModel() {
    }

    public BidCheckModel(String userName, BigDecimal amount) {
        this.userName = userName;
        this.amount = amount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
